-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Feb 2021 pada 13.19
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_lawyer`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_case_task`
--

CREATE TABLE `l_case_task` (
  `id_case` int(11) NOT NULL,
  `case_name` varchar(255) DEFAULT NULL,
  `case_type` enum('perdata','pidana') DEFAULT NULL,
  `lawyer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `edited_at` datetime DEFAULT NULL,
  `status` enum('1','2') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `l_case_task`
--

INSERT INTO `l_case_task` (`id_case`, `case_name`, `case_type`, `lawyer_id`, `created_at`, `edited_at`, `status`) VALUES
(1, 'Pencurian', 'pidana', 1, '2021-02-26 16:07:42', '2021-02-26 16:07:42', '1'),
(2, 'Penculikan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(3, 'Penculikan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(4, 'Kehilangan', 'pidana', 2, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(5, 'Kehilangan', 'pidana', 2, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(6, 'Kehilangan', 'pidana', 2, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(7, 'Kehilangan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(8, 'Pemalsuan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(9, 'Pembunuhan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(10, 'Pembunuhan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(11, 'Pembunuhan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1'),
(12, 'Pembunuhan', 'pidana', 1, '2021-02-26 17:18:07', '2021-02-26 17:18:07', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_client_member`
--

CREATE TABLE `l_client_member` (
  `id_client` int(11) NOT NULL,
  `client_name` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `client_phone` int(13) DEFAULT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `kab_kota` int(11) DEFAULT NULL,
  `kecamatan` int(11) DEFAULT NULL,
  `kelurahan` int(11) DEFAULT NULL,
  `avatar` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `client_ktp_scan` varchar(255) NOT NULL,
  `lawyer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `edited_at` datetime DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('1','2') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `l_client_member`
--

INSERT INTO `l_client_member` (`id_client`, `client_name`, `client_email`, `client_phone`, `client_address`, `province`, `kab_kota`, `kecamatan`, `kelurahan`, `avatar`, `client_ktp_scan`, `lawyer_id`, `created_at`, `edited_at`, `password`, `status`) VALUES
(1, 'Muara Firdaus', 'muarafirdaus713@gmail.com', 1380933893, 'Depok', 1, 1, 1, 1, 'default.jpg', '', 1, '2021-01-07 17:19:53', '2021-01-07 17:19:53', 'maura713', '1'),
(2, 'Rian Pradana', 'rianpradana@gmail.com', 88921083, 'Depok', 1, 1, 1, 1, 'default.jpg', '', 1, '2021-01-13 13:41:13', '2021-01-13 13:41:13', 'rianpradana', '1'),
(3, 'Adit', 'adit@gmail.com', 89028319, 'Jakarta', 1, 1, 1, 1, 'default.jpg', '', 1, '2021-01-13 13:42:54', '2021-01-13 13:42:54', 'adit123', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_duplik`
--

CREATE TABLE `l_duplik` (
  `id_duplik` int(11) NOT NULL,
  `duplik_name` varchar(255) NOT NULL,
  `case_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_gugatan_case`
--

CREATE TABLE `l_gugatan_case` (
  `id_gugatan` int(11) NOT NULL,
  `id_case` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `detail_gugatan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_gugatan_files`
--

CREATE TABLE `l_gugatan_files` (
  `id_files` int(11) NOT NULL,
  `title_file` varchar(255) NOT NULL,
  `gugatan_id` int(11) NOT NULL,
  `isDraft` int(11) NOT NULL,
  `files` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_kab_kota`
--

CREATE TABLE `l_kab_kota` (
  `kab_code` int(11) NOT NULL,
  `kab_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_kecamatan`
--

CREATE TABLE `l_kecamatan` (
  `kec_code` int(11) NOT NULL,
  `kec_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_kelurahan`
--

CREATE TABLE `l_kelurahan` (
  `kel_code` int(11) NOT NULL,
  `kel_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_member_detail`
--

CREATE TABLE `l_member_detail` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `ktp_number` varchar(255) DEFAULT NULL,
  `phone_number` int(13) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `full_address` text DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `kab_kota` int(11) DEFAULT NULL,
  `kecamatan` int(11) DEFAULT NULL,
  `kelurahan` int(11) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `package_type` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `edited_at` datetime DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `level` enum('admin','lawyer') NOT NULL DEFAULT 'lawyer'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `l_member_detail`
--

INSERT INTO `l_member_detail` (`id`, `full_name`, `ktp_number`, `phone_number`, `email`, `full_address`, `province`, `kab_kota`, `kecamatan`, `kelurahan`, `avatar`, `package_type`, `created_at`, `edited_at`, `password`, `status`, `level`) VALUES
(1, 'Ibrahim Kurniawan Sapuro, S.H.', '2091481201', 3821039, 'ibrahim@gmail.com', 'JAKARTA', 1, 1, 1, 1, 'default.jpg', 1, '2021-01-07 16:59:25', '2021-01-07 16:59:25', 'ibrahim123', 1, 'lawyer'),
(2, 'Super Admin', 'xxxxxxxxxx', 0, 'admin@admin.com', 'JAKARTA', 1, 1, 1, 1, 'default.jpg', 3, '2021-01-27 21:37:14', '2021-01-27 21:37:14', 'admin123', 1, 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_package`
--

CREATE TABLE `l_package` (
  `id_package` int(11) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `package_price` float DEFAULT NULL,
  `package_desc` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `edited_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `l_package`
--

INSERT INTO `l_package` (`id_package`, `package_name`, `package_price`, `package_desc`, `created_at`, `edited_at`, `status`) VALUES
(1, 'BASIC', 1000000, 'DESKRIPSI BASIC', '2021-01-28 15:53:12', '2021-01-28 15:53:12', 1),
(2, 'PROFESSIONAL', 2000000, 'DESKRIPSI PROFESSIONAL', '2021-01-28 15:53:12', '2021-01-28 15:53:12', 1),
(3, 'EXPERT', 3000000, 'DESKRIPSI EXPERT', '2021-01-28 15:53:12', '2021-01-28 15:53:12', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_pasal_kuhp`
--

CREATE TABLE `l_pasal_kuhp` (
  `id_pasal` int(11) NOT NULL,
  `pasal_no` int(11) NOT NULL,
  `ayat` int(11) NOT NULL,
  `isi` text NOT NULL,
  `tag` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_province`
--

CREATE TABLE `l_province` (
  `prov_code` int(11) NOT NULL,
  `prov_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `l_replik`
--

CREATE TABLE `l_replik` (
  `id_replik` int(11) NOT NULL,
  `replik_name` varchar(255) NOT NULL,
  `case_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `edited_at` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `l_case_task`
--
ALTER TABLE `l_case_task`
  ADD PRIMARY KEY (`id_case`);

--
-- Indeks untuk tabel `l_client_member`
--
ALTER TABLE `l_client_member`
  ADD PRIMARY KEY (`id_client`);

--
-- Indeks untuk tabel `l_duplik`
--
ALTER TABLE `l_duplik`
  ADD PRIMARY KEY (`id_duplik`);

--
-- Indeks untuk tabel `l_gugatan_case`
--
ALTER TABLE `l_gugatan_case`
  ADD PRIMARY KEY (`id_gugatan`);

--
-- Indeks untuk tabel `l_gugatan_files`
--
ALTER TABLE `l_gugatan_files`
  ADD PRIMARY KEY (`id_files`);

--
-- Indeks untuk tabel `l_kab_kota`
--
ALTER TABLE `l_kab_kota`
  ADD PRIMARY KEY (`kab_code`);

--
-- Indeks untuk tabel `l_kecamatan`
--
ALTER TABLE `l_kecamatan`
  ADD PRIMARY KEY (`kec_code`);

--
-- Indeks untuk tabel `l_kelurahan`
--
ALTER TABLE `l_kelurahan`
  ADD PRIMARY KEY (`kel_code`);

--
-- Indeks untuk tabel `l_member_detail`
--
ALTER TABLE `l_member_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `l_package`
--
ALTER TABLE `l_package`
  ADD PRIMARY KEY (`id_package`);

--
-- Indeks untuk tabel `l_pasal_kuhp`
--
ALTER TABLE `l_pasal_kuhp`
  ADD PRIMARY KEY (`id_pasal`);

--
-- Indeks untuk tabel `l_province`
--
ALTER TABLE `l_province`
  ADD PRIMARY KEY (`prov_code`);

--
-- Indeks untuk tabel `l_replik`
--
ALTER TABLE `l_replik`
  ADD PRIMARY KEY (`id_replik`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `l_case_task`
--
ALTER TABLE `l_case_task`
  MODIFY `id_case` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `l_client_member`
--
ALTER TABLE `l_client_member`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `l_duplik`
--
ALTER TABLE `l_duplik`
  MODIFY `id_duplik` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `l_gugatan_case`
--
ALTER TABLE `l_gugatan_case`
  MODIFY `id_gugatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `l_gugatan_files`
--
ALTER TABLE `l_gugatan_files`
  MODIFY `id_files` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `l_member_detail`
--
ALTER TABLE `l_member_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `l_package`
--
ALTER TABLE `l_package`
  MODIFY `id_package` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `l_pasal_kuhp`
--
ALTER TABLE `l_pasal_kuhp`
  MODIFY `id_pasal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `l_replik`
--
ALTER TABLE `l_replik`
  MODIFY `id_replik` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
