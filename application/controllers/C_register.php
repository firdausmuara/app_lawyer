<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_register extends CI_Controller {

	public function index()
	{
		$this->load->view('auth/v_header_auth');
		$this->load->view('auth/v_register');
		$this->load->view('auth/v_footer_auth');
	}
}
