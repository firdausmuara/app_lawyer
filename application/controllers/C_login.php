<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		// $this->load->model('M_login');
	}

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) {
			$this->load->view('auth/v_header_auth');
			$this->load->view('auth/v_login');
			$this->load->view('auth/v_footer_auth');
		} else {
			$this->auth();
		}
	}

	function auth()
	{
		$email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);
		$password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

		$cek_lawyer = $this->M_login->auth_lawyer($email, $password);
		// var_dump($cek_lawyer);die;

		if ($cek_lawyer->num_rows() > 0) { //jika login sebagai lawyer
			$data = $cek_lawyer->row_array();
			$this->session->set_userdata('masuk', TRUE);

			if ($data['level'] == 'admin') { //Akses admin
				$this->session->set_userdata('akses', '1');
				$this->session->set_userdata('ses_id', $data['id']);
				$this->session->set_userdata('ses_nama', $data['full_name']);
				$this->session->set_userdata('ses_ktp', $data['ktp_number']);
				$this->session->set_userdata('ses_phone', $data['phone_number']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_address', $data['full_address']);
				$this->session->set_userdata('ses_avatar', $data['avatar']);
				redirect('c_admin');
			} else { //akses lawyer
				$this->session->set_userdata('akses', '2');
				$this->session->set_userdata('ses_id', $data['id']);
				$this->session->set_userdata('ses_nama', $data['full_name']);
				$this->session->set_userdata('ses_ktp', $data['ktp_number']);
				$this->session->set_userdata('ses_phone', $data['phone_number']);
				$this->session->set_userdata('ses_email', $data['email']);
				$this->session->set_userdata('ses_address', $data['full_address']);
				$this->session->set_userdata('ses_avatar', $data['avatar']);
				redirect('c_lawyer');
			}
		} else { //jika login sebagai client
			$cek_client = $this->M_login->auth_client($email, $password);

			if ($cek_client->num_rows() > 0) {
				$data = $cek_client->row_array();
				$this->session->set_userdata('masuk', TRUE);

				$this->session->set_userdata('akses', '3');
				$this->session->set_userdata('ses_id', $data['id_client']);
				$this->session->set_userdata('ses_nama', $data['client_name']);
				$this->session->set_userdata('ses_email', $data['client_email']);
				$this->session->set_userdata('ses_phone', $data['client_phone']);
				$this->session->set_userdata('ses_address', $data['client_address']);
				$this->session->set_userdata('ses_avatar', $data['avatar']);
				$this->session->set_userdata('ses_ktp', $data['client_ktp_scan']);
				redirect('c_client');
			} else {  // jika email dan password tidak ditemukan atau salah
				$url = base_url('c_login');
				echo $this->session->set_flashdata('msg', 'Email Atau Password Salah');
				redirect($url);
			}
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		$url = base_url('');
		redirect($url);
	}
}
