<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url();
            redirect($url);
        }

        $this->load->model('M_lawyer');
        $this->load->model('M_client');
        $this->load->model('M_kasus');
    }

    public function index()
    {
        $this->load->view('templating/v_header');
        $this->load->view('admin/index');
        $this->load->view('templating/v_footer');
    }

    public function litigasi_kronologis()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_litigasi_kronologis');
        $this->load->view('templating/v_footer');
    }

    public function litigasi_kuasa()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_litigasi_kuasa');
        $this->load->view('templating/v_footer');
    }

    public function litigasi_gugatan()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_litigasi_gugatan');
        $this->load->view('templating/v_footer');
    }

    public function nonlitigasi()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_non_litigasi');
        $this->load->view('templating/v_footer');
    }

    public function template_surat_kuasa()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_template_surat_kuasa');
        $this->load->view('templating/v_footer');
    }

    public function profil()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_profile');
        $this->load->view('templating/v_footer');
    }

    public function pengaturan_lawyer()
    {
        $data['data'] = $this->M_lawyer->get_data()->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('admin/v_pengaturan_lawyer', $data);
        $this->load->view('templating/v_footer');
    }

    public function pengaturan_client()
    {
        $data['data'] = $this->M_client->get_data()->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('admin/v_pengaturan_client', $data);
        $this->load->view('templating/v_footer');
    }

    // Dasar Hukum
    public function dasar_hukum()
    {
        $data['data'] = $this->M_lawyer->get_dasar_hukum()->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('v_dasar_hukum', $data);
        $this->load->view('templating/v_footer');
    }
    // End of Dasar Hukum

    // Kasus
    public function kasus_baru()
    {
        $id_lawyer = $this->session->userdata('ses_id');
        $data['data'] = $this->M_client->get_data_by_lawyer($id_lawyer)->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('kasus/v_kasus_baru', $data);
        $this->load->view('templating/v_footer');
    }

    public function kasus_list()
    {
        $id_lawyer = $this->session->userdata('ses_id');
        $data['data'] = $this->M_kasus->get_data_by_lawyer($id_lawyer)->result();
        $data['all'] = $this->M_kasus->get_data()->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('kasus/v_list_kasus', $data);
        $this->load->view('templating/v_footer');
    }
    // End of Kasus

    // Search
    public function search()
    {
        $id_lawyer = $this->session->userdata('ses_id');

        // AMBIL DATA KEYWORD
        if ($this->input->post('submit')) {
            $data['keyword'] = $this->input->post('keyword');
            $this->session->set_userdata('keyword', $data['keyword']);
        } else {
            $data['keyword'] = $this->session->userdata('keyword');
        }

        if ($this->session->userdata('akses') == '2') { // LAWYER AKSES
            // CONFIG
            $this->db->like('case_name', $data['keyword']);
            $this->db->or_like('case_type', $data['keyword']);
            $this->db->from('l_case_task');
            $this->db->where('lawyer_id', $id_lawyer);
            $config['total_rows'] = $this->db->count_all_results();
            $config['per_page'] = 5;
        } elseif ($this->session->userdata('akses') == '1') { // ADMIN AKSES
            // CONFIG
            $this->db->like('case_name', $data['keyword']);
            $this->db->or_like('case_type', $data['keyword']);
            $this->db->from('l_case_task');
            $config['total_rows'] = $this->db->count_all_results();
            $config['per_page'] = 5;
        }

        // INITIALIZE
        $this->pagination->initialize($config);

        $data['total_rows'] = $config['total_rows'];
        $data['start'] = $this->uri->segment(3);
        $data['data'] = $this->M_kasus->search_data_by_lawyer($id_lawyer, $config['per_page'], $data['start'], $data['keyword'])->result();
        $data['all'] = $this->M_kasus->search_data($config['per_page'], $data['start'], $data['keyword'])->result();

        $this->load->view('templating/v_header', $data);
        $this->load->view('v_list_search', $data);
        $this->load->view('templating/v_footer');
    }
    // End of Search

    public function chat()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_chatting');
        $this->load->view('templating/v_footer');
    }

    public function schedule()
    {
        $this->load->view('templating/v_header');
        $this->load->view('v_schedule');
        $this->load->view('templating/v_footer');
    }
}
