<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('home/v_header');
        $this->load->view('home/index');
        $this->load->view('home/v_footer');
    }
}
