<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_lawyer extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		//validasi jika user belum login
		if ($this->session->userdata('masuk') != TRUE) {
			$url = base_url();
			redirect($url);
		}

		$this->load->model('M_lawyer');
	}

	public function index()
	{
		$this->load->view('templating/v_header');
		$this->load->view('lawyer/index');
		$this->load->view('templating/v_footer');
	}

	public function list_data()
	{
		$data['data'] = $this->M_lawyer->get_data()->result();
		$this->load->view('templating/v_header', $data);
		$this->load->view('lawyer/v_list_lawyer', $data);
		$this->load->view('templating/v_footer');
	}

	public function tambah()
	{
		$this->load->view('templating/v_header');
		$this->load->view('lawyer/v_tambah_lawyer');
		$this->load->view('templating/v_footer');
	}
}
