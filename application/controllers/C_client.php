<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_client extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        if ($this->session->userdata('masuk') != TRUE) {
            $url = base_url();
            redirect($url);
        }

        $this->load->model('M_client');
    }

    public function index()
    {
        $this->load->view('templating/v_header');
        $this->load->view('client/index');
        $this->load->view('templating/v_footer');
    }

    public function list_data()
    {
        // $id_lawyer = $this->session->userdata('ses_id');
        // var_dump($id_lawyer); die;
        // $data['data'] = $this->M_client->get_data_by_lawyer($id_lawyer)->result();
        // $data['all'] = $this->M_client->get_data()->result();
        $this->load->view('templating/v_header');
        $this->load->view('client/v_list_client');
        $this->load->view('templating/v_footer');
    }

    public function new_client()
    {
        $id_lawyer = $this->session->userdata('ses_id');
        // var_dump($id_lawyer); die;
        $data['data'] = $this->M_client->get_data_by_lawyer($id_lawyer)->result();
        $data['all'] = $this->M_client->get_data()->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('client/v_new_client', $data);
        $this->load->view('templating/v_footer');
    }

    public function detail_client($id = null)
    {
        $data['data'] = $this->M_client->get_bukti_by_id($id)->result();
        $this->load->view('templating/v_header', $data);
        $this->load->view('client/v_detail_client', $data);
        $this->load->view('templating/v_footer');
    }

    public function tambah()
    {
        $this->load->view('templating/v_header');
        $this->load->view('client/v_form_client');
        $this->load->view('templating/v_footer');
    }
}
