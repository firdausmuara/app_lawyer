      <main role="main" class="main-content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="row align-items-center mb-3 border-bottom no-gutters">
                <div class="col">
                  <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" role="tab" onclick="openTab('Kronologis')">Lembar Kronologis</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" role="tab" onclick="openTab('Kuasa')">Surat Kuasa</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" role="tab" onclick="openTab('Gugatan')">Gugatan</a>
                    </li>
                  </ul>
                </div>
            </div>
            <!-- The Froala editor gets initialized inside of this DIV element -->
            <div id="Kronologis" class="editor froala-editor">Ini Lembar Kronologis</div>
            <div id="Kuasa" class="editor froala-editor" style="display:none;">Ini Surat Kuasa</div>
            <div id="Gugatan" class="editor froala-editor" style="display:none;">Ini Gugatan</div>
          </div> <!-- .row -->
        </div> <!-- .container-fluid -->
        <!-- Modal Shortcut -->