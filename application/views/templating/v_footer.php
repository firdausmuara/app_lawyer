        <!-- Modal Shortcut -->
        <div class="modal fade modal-shortcut modal-slide" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="defaultModalLabel">Shortcuts</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body px-5">
                <div class="row align-items-center">
                  <div class="col-6 text-center">
                    <div class="squircle bg-success justify-content-center">
                      <i class="fe fe-cpu fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Control area</p>
                  </div>
                  <div class="col-6 text-center">
                    <div class="squircle bg-primary justify-content-center">
                      <i class="fe fe-activity fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Activity</p>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-6 text-center">
                    <div class="squircle bg-primary justify-content-center">
                      <i class="fe fe-droplet fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Droplet</p>
                  </div>
                  <div class="col-6 text-center">
                    <div class="squircle bg-primary justify-content-center">
                      <i class="fe fe-upload-cloud fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Upload</p>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-6 text-center">
                    <div class="squircle bg-primary justify-content-center">
                      <i class="fe fe-users fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Users</p>
                  </div>
                  <div class="col-6 text-center">
                    <div class="squircle bg-primary justify-content-center">
                      <i class="fe fe-settings fe-32 align-self-center text-white"></i>
                    </div>
                    <p>Settings</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Litigasi -->
        <div class="modal fade modal-litigasi modal-slide" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <select class="custom-select" id="select-litigasi">
                  <option value="0" selected>Pilih Client</option>
                  <option value="1">Muara Firdaus</option>
                  <option value="2">Rian Pradana</option>
                  <option value="3">Cahyo Sasongko</option>
                </select>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body px-5" id="sideModal" hidden>
                <div class="row align-items-center">
                  <div class="col-6 text-center">
                    <!-- <a href="<?php echo base_url() ?>c_dashboard/litigasi_kronologis">
                      <div class="squircle bg-success justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a> -->
                    <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                      <div class="squircle bg-success justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>L. Kronologis</p>
                  </div>
                  <div class="col-6 text-center">
                    <!-- <a href="<?php echo base_url() ?>c_dashboard/litigasi_kuasa">
                      <div class="squircle bg-primary justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a> -->
                    <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                      <div class="squircle bg-primary justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>Surat Kuasa</p>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-12 text-center">
                    <!-- <a href="<?php echo base_url() ?>c_dashboard/litigasi_gugatan">
                      <div class="squircle bg-warning justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a> -->
                    <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                      <div class="squircle bg-warning justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>Gugatan</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal Non Litigasi -->
        <!-- <div class="modal fade modal-nonlitigasi modal-slide" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <select class="custom-select" id="custom-select">
                  <option selected>Pilih Client</option>
                  <option value="1">Muara Firdaus</option>
                  <option value="2">Rian Pradana</option>
                  <option value="3">Cahyo Sasongko</option>
                </select>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body px-5">
                <div class="row align-items-center">
                  <div class="col-6 text-center">
                    <a href="<?php echo base_url() ?>c_dashboard/litigasi">
                      <div class="squircle bg-success justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>L. Kronologis</p>
                  </div>
                  <div class="col-6 text-center">
                    <a href="<?php echo base_url() ?>c_dashboard/litigasi">
                      <div class="squircle bg-primary justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>Surat Kuasa</p>
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col-12 text-center">
                    <a href="<?php echo base_url() ?>c_dashboard/litigasi">
                      <div class="squircle bg-warning justify-content-center">
                        <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                      </div>
                    </a>
                    <p>Gugatan</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- Form Modal Litigasi -->
        <div class="modal fade" id="varyModal" tabindex="-1" role="dialog" aria-labelledby="varyModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="varyModalLabel">Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nama Tergugat</label>
                    <input type="text" class="form-control" id="recipient-name">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label">Alamat Tergugat</label>
                    <textarea class="form-control" id="message-text"></textarea>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="<?php echo base_url() ?>c_admin/litigasi_kuasa" type="button" class="btn mb-2 btn-primary">Simpan</a>
              </div>
            </div>
          </div>
        </div>

        <!-- Form Modal Dokumen Menu -->
        <div class="modal fade" id="DocModal" tabindex="-1" role="dialog" aria-labelledby="DocModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="DocModalLabel">Pilih Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <select class="custom-select" id="select-litigasi">
                    <option value="0" selected>Pilih Client</option>
                    <option value="1">Muara Firdaus</option>
                    <option value="2">Rian Pradana</option>
                    <option value="3">Cahyo Sasongko</option>
                  </select>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Tutup</button>
                <a href="<?php echo base_url() ?>c_client/detail_client/1" type="button" class="btn mb-2 btn-primary">Simpan</a>
              </div>
            </div>
          </div>
        </div>
        </main> <!-- main -->
        </div> <!-- .wrapper -->
        <script src="<?php echo base_url() ?>tinydash_assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/popper.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/moment.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/simplebar.min.js"></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/daterangepicker.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.stickOnScroll.js'></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/tinycolor-min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/config.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/d3.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/topojson.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/datamaps.all.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/datamaps-zoomto.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/datamaps.custom.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/Chart.min.js"></script>
        <script>
          /* defind global options */
          Chart.defaults.global.defaultFontFamily = base.defaultFontFamily;
          Chart.defaults.global.defaultFontColor = colors.mutedColor;
        </script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/fullcalendar.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/fullcalendar.custom.js'></script>
        <script>
          /** full calendar */
          var calendarEl = document.getElementById('calendar');
          if (calendarEl) {
            document.addEventListener('DOMContentLoaded', function() {
              var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['dayGrid', 'timeGrid', 'list', 'bootstrap'],
                timeZone: 'UTC',
                themeSystem: 'bootstrap',
                header: {
                  left: 'today, prev, next',
                  center: 'title',
                  right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                buttonIcons: {
                  prev: 'fe-arrow-left',
                  next: 'fe-arrow-right',
                  prevYear: 'left-double-arrow',
                  nextYear: 'right-double-arrow'
                },
                weekNumbers: true,
                eventLimit: true, // allow "more" link when too many events
                events: 'https://fullcalendar.io/demo-events.json'
              });
              calendar.render();
            });
          }
        </script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/gauge.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/apexcharts.min.js"></script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/apexcharts.custom.js"></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.mask.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/select2.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.steps.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.validate.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.timepicker.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/dropzone.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/uppy.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/quill.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/jquery.dataTables.min.js'></script>
        <script src='<?php echo base_url() ?>tinydash_assets/js/dataTables.bootstrap4.min.js'></script>
        <script>
          $('#dataTable-1').DataTable({
            autoWidth: true,
            "lengthMenu": [
              [16, 32, 64, -1],
              [16, 32, 64, "All"]
            ]
          });
        </script>
        <script>
          $('.select2').select2({
            theme: 'bootstrap4',
          });
          $('.select2-multi').select2({
            multiple: true,
            theme: 'bootstrap4',
          });
          $('.drgpicker').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            showDropdowns: true,
            locale: {
              format: 'YYYY-MM-DD hh:mm'
            }
          });
          $('.time-input').timepicker({
            'scrollDefault': 'now',
            'zindex': '9999' /* fix modal open */
          });
          /** date range picker */
          if ($('.datetimes').length) {
            $('.datetimes').daterangepicker({
              timePicker: true,
              startDate: moment().startOf('hour'),
              endDate: moment().startOf('hour').add(32, 'hour'),
              locale: {
                format: 'M/DD hh:mm A'
              }
            });
          }
          var start = moment().subtract(29, 'days');
          var end = moment();

          function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
          }
          $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
          }, cb);
          cb(start, end);
          $('.input-placeholder').mask("00/00/0000", {
            placeholder: "__/__/____"
          });
          $('.input-zip').mask('00000-000', {
            placeholder: "____-___"
          });
          $('.input-money').mask("#.##0,00", {
            reverse: true
          });
          $('.input-phoneus').mask('(000) 000-0000');
          $('.input-mixed').mask('AAA 000-S0S');
          $('.input-ip').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
            translation: {
              'Z': {
                pattern: /[0-9]/,
                optional: true
              }
            },
            placeholder: "___.___.___.___"
          });
          // editor
          var editor = document.getElementById('editor');
          if (editor) {
            var toolbarOptions = [
              [{
                'font': []
              }],
              [{
                'header': [1, 2, 3, 4, 5, 6, false]
              }],
              ['bold', 'italic', 'underline', 'strike'],
              ['blockquote', 'code-block'],
              [{
                  'header': 1
                },
                {
                  'header': 2
                }
              ],
              [{
                  'list': 'ordered'
                },
                {
                  'list': 'bullet'
                }
              ],
              [{
                  'script': 'sub'
                },
                {
                  'script': 'super'
                }
              ],
              [{
                  'indent': '-1'
                },
                {
                  'indent': '+1'
                }
              ], // outdent/indent
              [{
                'direction': 'rtl'
              }], // text direction
              [{
                  'color': []
                },
                {
                  'background': []
                }
              ], // dropdown with defaults from theme
              [{
                'align': []
              }],
              ['clean'] // remove formatting button
            ];
            var quill = new Quill(editor, {
              modules: {
                toolbar: toolbarOptions
              },
              theme: 'snow'
            });
          }
          // Example starter JavaScript for disabling form submissions if there are invalid fields
          (function() {
            'use strict';
            window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                  }
                  form.classList.add('was-validated');
                }, false);
              });
            }, false);
          })();
        </script>
        <script>
          var uptarg = document.getElementById('drag-drop-area');
          if (uptarg) {
            var uppy = Uppy.Core().use(Uppy.Dashboard, {
              inline: true,
              target: uptarg,
              proudlyDisplayPoweredByUppy: false,
              theme: 'dark',
              width: 770,
              height: 210,
              plugins: ['Webcam']
            }).use(Uppy.Tus, {
              endpoint: 'https://master.tus.io/files/'
            });
            uppy.on('complete', (result) => {
              console.log('Upload complete! We’ve uploaded these files:', result.successful)
            });
          }
        </script>
        <script src="<?php echo base_url() ?>tinydash_assets/js/apps.js"></script>
        <script>
          new FroalaEditor('div.froala-editor', {
            documentReady: true
          })
        </script>

        <script>
          // Edit Data
          function submitClient() {
              // PRIBADI
                  var id_client = $("#id_client").val();
                  var client_name = $("#client_name").val();
                  var client_email = $("#client_email").val();
                  var client_ktp = $("#client_ktp").val();
                  var client_phone = $("#client_phone").val();
                  var client_address = $("#client_address").val();
                  var province = '1';
                  var kab_kota = '1';
                  var kecamatan = '1';
                  var kelurahan = '1';
                  var myfiles = document.getElementById("client_ktp_scan");
                  var client_ktp_scan = myfiles.files;
                  var lawyer_id = <?= $this->session->userdata('ses_id') ?>;
                  var date = Date.now();
                  var password = 'default123';
                  var status = '2';
                  var avatar = 'default.jpg';

              // BADAN HUKUM
                  var id_bakum = $("#id_bakum").val();
                  var nama_bakum = $("#nama_bakum").val();
                  var alamat_bakum = $("#alamat_bakum").val();
                  var provinsi_bakum = '1';
                  var kab_kota_bakum = '1';
                  var kecamatan_bakum = '1';
                  var kelurahan_bakum = '1';
                  var no_akta_bakum = $("#no_akta_bakum").val();
                  var tgl_akta_bakum = $("#tgl_akta_bakum").val();
                  var nama_notaris_bakum = $("#nama_notaris_bakum").val();
                  var sk_menkumham_bakum = $("#sk_menkumham_bakum").val();
                  var dokumen_bakum = 'scan.pdf';

              var dok = new FormData();
              // PRIBADI
                  dok.append('id_client', id_client)
                  dok.append('client_name', client_name)
                  dok.append('client_email', client_email)
                  dok.append('client_ktp', client_ktp)
                  dok.append('client_phone', client_phone)
                  dok.append('client_address', client_address)
                  dok.append('province', province)
                  dok.append('kab_kota', kab_kota)
                  dok.append('kecamatan', kecamatan)
                  dok.append('kelurahan', kelurahan)
                  dok.append('client_ktp_scan', client_ktp_scan)
                  dok.append('lawyer_id', lawyer_id)
                  dok.append('created_at', date)
                  dok.append('edited_at', date)
                  dok.append('password', password)
                  dok.append('status', status)
                  dok.append('avatar', avatar)

              // BADAN HUKUM
                  dok.append('id_bakum', id_bakum)
                  dok.append('nama_bakum', nama_bakum)
                  dok.append('alamat_bakum', alamat_bakum)
                  dok.append('provinsi_bakum', provinsi_bakum)
                  dok.append('kab_kota_bakum', kab_kota_bakum)
                  dok.append('kecamatan_bakum', kecamatan_bakum)
                  dok.append('kelurahan_bakum', kelurahan_bakum)
                  dok.append('no_akta_bakum', no_akta_bakum)
                  dok.append('tgl_akta_bakum', tgl_akta_bakum)
                  dok.append('nama_notaris_bakum', nama_notaris_bakum)
                  dok.append('sk_menkumham_bakum', sk_menkumham_bakum)
                  dok.append('dokumen_bakum', dokumen_bakum)

                  for (var pair of dok.entries()) {
                      console.log(pair[0]+ ', ' + pair[1]); 
                  }
              $.ajax({
                  url: "https://be-applawyer.demodantest.in/api/client/update",
                  method: 'POST',
                  data: dok,
                  dataType: 'json',
                  processData: false,
                  contentType: false,
                  beforeSend: function () {
                  },
                  success: function (dt) {
                      console.log('Sukses Update',dt)
                      window.location.href= 'new_client';
                      return
                  }
              })
            }

        </script>
        </body>

        </html>