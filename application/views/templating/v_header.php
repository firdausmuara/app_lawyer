<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Lawyer">
  <meta name="author" content="Muara Firdaus">
  <link rel="icon" href="<?php echo base_url() ?>tinydash_assets/assets/images/logo.svg">
  <title>App Lawyer</title>
  <!-- Simple bar CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/simplebar.css">
  <!-- Fonts CSS -->
  <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <!-- Icons CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/feather.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/select2.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/dropzone.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/uppy.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/jquery.steps.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/jquery.timepicker.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/quill.snow.css">
  <!-- FullCalendar CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/fullcalendar.css">
  <!-- Date Range Picker CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/daterangepicker.css">
  <!-- App CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/app-light.css" id="lightTheme">
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/app-dark.css" id="darkTheme" disabled>
  <!-- CSS EDITOR -->
  <link href="<?php echo base_url() ?>css_editor/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
  <!-- CSS DATATABLE -->
  <link rel="stylesheet" href="<?php echo base_url() ?>tinydash_assets/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>css_editor/js/froala_editor.pkgd.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <!-- DataTable -->
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
  <!-- SWAL -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body class="vertical  light  ">
  <div class="wrapper">
    <nav class="topnav navbar navbar-light">
      <button type="button" class="navbar-toggler text-muted mt-2 p-0 mr-3 collapseSidebar">
        <i class="fe fe-menu navbar-toggler-icon"></i>
      </button>
      <form class="form-inline mr-auto searchform text-muted" method="POST" action="<?= base_url(); ?>c_admin/search">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search Keyword" autocomplete="off" autofocus name="keyword">
          <input name="submit" value="submit" hidden>
          <div class="input-group-append">
            <button class="input-group-text" type="submit" id="basic-addon2"><i class="fe fe-search"></i></button>
          </div>
        </div>
        <!-- <input class="form-control mr-sm-2 bg-transparent border-0 pl-4 text-muted" type="search" placeholder="Search Keyword" autofocus> -->
      </form>
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link text-muted my-2" href="<?= base_url(); ?>c_admin/chat">
            <i class="fe fe-message-circle fe-16"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-muted my-2" href="#" id="modeSwitcher" data-mode="light">
            <i class="fe fe-sun fe-16"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-muted my-2" href="./#" data-toggle="modal" data-target=".modal-shortcut">
            <span class="fe fe-grid fe-16"></span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-muted pr-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="avatar avatar-sm mt-2">
              <img src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-1.jpg" alt="..." class="avatar-img rounded-circle">
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="<?php echo base_url() ?>c_admin/profil">Profil</a>
            <a class="dropdown-item" href="<?php echo base_url() ?>c_login/logout">Log Out</a>
          </div>
        </li>
      </ul>
    </nav>

    <!-- View Menu Akses -->
    <?php $this->load->view('templating/v_menu'); ?>