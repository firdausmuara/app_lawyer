<aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
    <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
        <i class="fe fe-x"><span class="sr-only"></span></i>
    </a>
    <nav class="vertnav navbar navbar-light">
        <!-- nav bar -->
        <div class="w-100 mb-4 d-flex">
            <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="<?php echo base_url() ?>c_dashboard">
                <svg version="1.1" id="logo" class="navbar-brand-img brand-sm" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
                    <g>
                        <polygon class="st0" points="78,105 15,105 24,87 87,87 	" />
                        <polygon class="st0" points="96,69 33,69 42,51 105,51 	" />
                        <polygon class="st0" points="78,33 15,33 24,15 87,15 	" />
                    </g>
                </svg>
            </a>
        </div>
        <ul class="navbar-nav flex-fill w-100 mb-2">
            <!-- Akses untuk Admin -->
            <?php if ($this->session->userdata('akses') == '1') : ?>
                <li class="nav-item w-100">
                    <a class="nav-link" href="<?php echo base_url() ?>c_admin">
                        <i class="fe fe-home fe-16"></i>
                        <span class="ml-3 item-text">Dashboard</span><span class="sr-only">(current)</span>
                    </a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a href="#lawyer" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-users fe-16"></i>
                        <span class="ml-3 item-text">Lawyers</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="lawyer">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_lawyer/list"><span class="ml-1 item-text">List</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_lawyer/tambah"><span class="ml-1 item-text">Tambah Data</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#client" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-users fe-16"></i>
                        <span class="ml-3 item-text">Client</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="client">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_client/list"><span class="ml-1 item-text">List</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_client/tambah"><span class="ml-1 item-text">Tambah Data</span></a>
                        </li>
                    </ul>
                </li> -->
                <li class="nav-item dropdown">
                    <a href="#masterData" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-monitor fe-16"></i>
                        <span class="ml-3 item-text">Master Data</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="masterData">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Dasar Hukum</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/template_surat_kuasa"><span class="ml-1 item-text">Template D. Surat Kuasa</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="#"><span class="ml-1 item-text">Template D. Kronologis</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#kasus" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-clock fe-16"></i>
                        <span class="ml-3 item-text">Kasus</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="kasus">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/kasus_baru"><span class="ml-1 item-text">Baru</span></a>
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/kasus_list"><span class="ml-1 item-text">List</span></a>
                            <!-- <a class="nav-link pl-3" href="#" data-toggle="modal" data-target=".modal-nonlitigasi"><span class="ml-1 item-text">Non Litigasi</span></a> -->
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#akun" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-settings fe-16"></i>
                        <span class="ml-3 item-text">Pengaturan Akun</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="akun">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/pengaturan_client"><span class="ml-1 item-text">Pengaturan Client</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/pengaturan_lawyer"><span class="ml-1 item-text">Pengaturan Lawyer</span></a>
                        </li>
                    </ul>
                </li>
                <!-- Akses untuk Lawyer -->
            <?php elseif ($this->session->userdata('akses') == '2') : ?>
                <li class="nav-item w-100">
                    <a class="nav-link" href="<?php echo base_url() ?>c_lawyer">
                        <i class="fe fe-home fe-16"></i>
                        <span class="ml-3 item-text">Dashboard</span><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#client" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-users fe-16"></i>
                        <span class="ml-3 item-text">Client</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="client">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_client/new_client"><span class="ml-1 item-text">Prospect Client</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_client/list_data"><span class="ml-1 item-text">Listed Client</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#masterData" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-monitor fe-16"></i>
                        <span class="ml-3 item-text">Legal Research</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="masterData">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Dasar Hukum</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/template_surat_kuasa"><span class="ml-1 item-text">Yurisprudensi</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a href="#dokumen" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-archive fe-16"></i>
                        <span class="ml-3 item-text">Dokumen</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="dokumen">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="#" data-toggle="modal" data-target="#DocModal" data-whatever="@mdo"><span class="ml-1 item-text">Lembar Kronologis</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="#" data-toggle="modal" data-target="#DocModal" data-whatever="@mdo"><span class="ml-1 item-text">List Bukti</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="#" data-toggle="modal" data-target="#DocModal" data-whatever="@mdo"><span class="ml-1 item-text">Legal Research</span></a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item w-100">
                    <a class="nav-link" href="<?php echo base_url() ?>c_lawyer">
                        <i class="fe fe-archive fe-16"></i>
                        <span class="ml-3 item-text">Administrasi Keuangan</span><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#legal_action" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-archive fe-16"></i>
                        <span class="ml-3 item-text">Legal Action</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="legal_action">
                        <li class="nav-item">
                            <a href="#non_litigasi_l" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link"><span class="ml-1 item-text">Non Litigasi</span></a>
                            <ul class="collapse list-unstyled pl-4 w-100" id="non_litigasi_l">
                                <li class="nav-item">
                                    <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Somasi</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Minutes of Meeting</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Perdamaian</span></a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#litigasi_l" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link"><span class="ml-1 item-text">Litigasi</span></a>
                            <ul class="collapse list-unstyled pl-4 w-100" id="litigasi_l">
                                <li class="nav-item">
                                    <a href="#litigasi_penggugat" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link"><span class="ml-1 item-text">Penggugat</span></a>
                                    <ul class="collapse list-unstyled pl-4 w-100" id="litigasi_penggugat">
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/litigasi_gugatan"><span class="ml-1 item-text">Gugatan</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Replik</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Daftar Bukti</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Pemeriksaan Saksi</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Simpulan</span></a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#litigasi_tergugat" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link"><span class="ml-1 item-text">Tergugat</span></a>
                                    <ul class="collapse list-unstyled pl-4 w-100" id="litigasi_tergugat">
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/litigasi_gugatan"><span class="ml-1 item-text">Jawaban / Eksepsi</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Duplik</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Daftar Bukti</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Pemeriksaan Saksi</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Simpulan</span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item w-100">
                    <a class="nav-link" href="<?php echo base_url() ?>c_admin/schedule">
                        <i class="fe fe-calendar fe-16"></i>
                        <span class="ml-3 item-text">Schedule</span><span class="sr-only">(current)</span>
                    </a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a href="#kasus" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-clock fe-16"></i>
                        <span class="ml-3 item-text">Kasus</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="kasus">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/kasus_baru"><span class="ml-1 item-text">Baru</span></a>
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/kasus_list"><span class="ml-1 item-text">List</span></a>
                        </li>
                    </ul>
                </li> -->
                <!-- Akses untuk Lawyer -->
            <?php else : ?>
                <li class="nav-item w-100">
                    <a class="nav-link" href="<?php echo base_url() ?>c_client">
                        <i class="fe fe-home fe-16"></i>
                        <span class="ml-3 item-text">Dashboard</span><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#masterData" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                        <i class="fe fe-monitor fe-16"></i>
                        <span class="ml-3 item-text">Master Data</span>
                    </a>
                    <ul class="collapse list-unstyled pl-4 w-100" id="masterData">
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/dasar_hukum"><span class="ml-1 item-text">Dasar Hukum</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="<?php echo base_url() ?>c_admin/template_surat_kuasa"><span class="ml-1 item-text">Template D. Surat Kuasa</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link pl-3" href="#"><span class="ml-1 item-text">Template D. Kronologis</span></a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </nav>
</aside>