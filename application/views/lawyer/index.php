<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row align-items-center mb-2">
                    <div class="col">
                        <h2 class="h5 page-title">Welcome!</h2>
                    </div>
                    <div class="col-auto">
                        <form class="form-inline">
                            <div class="form-group d-none d-lg-inline">
                                <label for="reportrange" class="sr-only">Date Ranges</label>
                                <div id="reportrange" class="px-2 py-2 text-muted">
                                    <span class="small"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-sm"><span class="fe fe-refresh-ccw fe-16 text-muted"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card shadow my-4">
                    <div class="card-body">
                        <div class="row align-items-center my-4">
                            <div class="col-md-4">
                                <div class="mx-4">
                                    <?php if ($this->session->userdata('akses') == '1') : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Admin</strong><br />
                                    <?php elseif ($this->session->userdata('akses') == '2') : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Lawyer</strong><br />
                                    <?php else : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Client</strong><br />
                                    <?php endif; ?>
                                    <h3><?php echo $this->session->userdata('ses_nama'); ?></h3>
                                    <p class="text-muted"><?php echo $this->session->userdata('ses_address'); ?></p>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6">
                                        <div class="p-4">
                                            <p class="small text-uppercase text-muted mb-0">Total Client</p>
                                            <span class="h2 mb-0">10</span>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="p-4">
                                            <p class="small text-uppercase text-muted mb-0">Total Kasus</p>
                                            <span class="h2 mb-0">10</span>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="p-4">
                                            <a href="<?php echo base_url() ?>c_client/tambah?aksi=tambah" class="btn btn-sm btn-primary">Tambah Client</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="mr-4">
                                    <canvas id="pieChartjs" width="400" height="300"></canvas>
                                </div>
                            </div> <!-- .col-md-8 -->
                        </div> <!-- end section -->
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
                <div class="row">
                    <!-- Recent orders -->
                    <div class="col-md-8">
                        <div class="card shadow eq-card">
                            <div class="card-header">
                                <strong class="card-title">Listed Client</strong>
                                <a class="float-right small text-muted" href="<?php echo base_url() ?>c_client/list_data">View all</a>
                            </div>
                            <div class="card-body">
                                <table class="table table-hover table-borderless table-striped mt-n3 mb-n1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Client</th>
                                            <th>Email Client</th>
                                            <th>Nomor Client</th>
                                            <th>Alamat</th>
                                            <th>Status Surat</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list_client_by_lawyer">
                                    </tbody>
                                </table>
                            </div> <!-- .card-body -->
                        </div> <!-- .card -->
                    </div> <!-- / .col-md-8 -->
                    <!-- Recent Activity -->
                    <div class="col-md-4">
                        <div class="card shadow eq-card timeline">
                            <div class="card-header">
                                <strong class="card-title">Recent Activity</strong>
                                <a class="float-right small text-muted" href="#!">View all</a>
                            </div>
                            <div class="card-body" data-simplebar style="height: 360px; overflow-y: auto; overflow-x: hidden;">
                                <div class="pb-3 timeline-item item-primary">
                                    <div class="pl-5">
                                        <div class="mb-1 small"><strong>@Brown Asher</strong><span class="text-muted mx-2">Just create new layout Index, form,
                                                table</span><strong>Tiny Admin</strong></div>
                                        <p class="small text-muted">Creative Design <span class="badge badge-light">1h
                                                ago</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="pb-3 timeline-item item-warning">
                                    <div class="pl-5">
                                        <div class="mb-3 small"><strong>@Fletcher Everett</strong><span class="text-muted mx-2">created new group for</span><strong>Tiny
                                                Admin</strong></div>
                                        <ul class="avatars-list mb-2">
                                            <li>
                                                <a href="#!" class="avatar avatar-sm">
                                                    <img alt="..." class="avatar-img rounded-circle" src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-1.jpg">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#!" class="avatar avatar-sm">
                                                    <img alt="..." class="avatar-img rounded-circle" src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-4.jpg">
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#!" class="avatar avatar-sm">
                                                    <img alt="..." class="avatar-img rounded-circle" src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-3.jpg">
                                                </a>
                                            </li>
                                        </ul>
                                        <p class="small text-muted">Front-End Development <span class="badge badge-light">1h ago</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="pb-3 timeline-item item-success">
                                    <div class="pl-5">
                                        <div class="mb-2 small"><strong>@Kelley Sonya</strong><span class="text-muted mx-2">has commented on</span><strong>Advanced
                                                table</strong></div>
                                        <div class="card d-inline-flex mb-2">
                                            <div class="card-body bg-light small py-2 px-3"> Lorem ipsum dolor sit amet,
                                                consectetur adipiscing elit. </div>
                                        </div>
                                        <p class="small text-muted">Back-End Development <span class="badge badge-light">1h ago</span>
                                        </p>
                                    </div>
                                </div>
                            </div> <!-- / .card-body -->
                        </div> <!-- / .card -->
                    </div> <!-- / .col-md-3 -->
                </div> <!-- end section -->
            </div>
            <!-- The Froala editor gets initialized inside of this DIV element -->
            <!-- <div id="froala-editor"></div> -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->
    <!-- Modal Shortcut -->

    <script>
        $(document).ready(function() {
            getDataList();
            setTimeout(() => {
                $("[data-toggle='tooltip']").tooltip()
                $('#dtTable').DataTable()
            }, 500);
        })

        function getDataList() {
            var id_lawyer = <?php echo $this->session->userdata('ses_id'); ?>
            // console.log(id_lawyer);
            $.ajax({
                url: 'https://be-applawyer.demodantest.in/api/client/lawyer',
                method: 'GET',
                data: {
                    id_lawyer: id_lawyer,
                    status: '1'
                },
                dataType: 'json',
                success: function(data) {
                    $("#list_client_by_lawyer").html('');
                    var html = ' ';
                    console.log('data by lawyer', data);
                    var no = 1;
                    for (var i in data) {
                        const law = data[i];
                        //  console.log(law);
                        if (law.status == 1) {
                            var status = 'Aktif'
                        } else {
                            var status = 'Tidak Aktif'
                        }

                        html += `<tr>
                                        <td>${no}</td>
                                        <td>
                                        <a href="<?php echo base_url() ?>c_client/detail_client/${law.id_client}">${law.client_name}</a>
                                        </td>
                                        <td>${law.client_email}</td>
                                        <td>${law.client_phone}</td>
                                        <td>${law.client_address}</td>
                                        <td>${status}</td>
                                    </tr>`;
                        no++;
                    }
                    $("#list_client_by_lawyer").append(html);
                }
            });
        }
    </script>