<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="page-title">Tambah Data</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profil</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-akun-tab" data-toggle="pill" href="#pills-akun" role="tab" aria-controls="pills-akun" aria-selected="true">Akun</a>
                                    </li>
                                </ul>
                                <form action="<?php echo base_url() ?>c_lawyer">
                                    <div class="tab-content mb-1" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="form-group">
                                                <label for="inputNama">Nama Lengkap</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="inputNama" placeholder="Nama Lengkap">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNomor">Nomor KTP</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="inputNomor" placeholder="Nomor KTP">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNomor">Nomor Handphone</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="inputNomor" placeholder="08xxxxxxx">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputAddress">Alamat Lengkap</label>
                                                <textarea name="client_address" class="form-control" id="inputAddress" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455"></textarea>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Provinsi</label>
                                                    <select id="inputState5" class="form-control">
                                                        <option selected>Choose...</option>
                                                        <option>...</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Kota</label>
                                                    <select id="inputState5" class="form-control">
                                                        <option selected>Choose...</option>
                                                        <option>...</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputState">Kecamatan</label>
                                                    <select id="inputState5" class="form-control">
                                                        <option selected>Choose...</option>
                                                        <option>...</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputZip">Kelurahan</label>
                                                    <select id="inputState5" class="form-control">
                                                        <option selected>Choose...</option>
                                                        <option>...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="customFile">Foto/Avatar</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="customFile">Foto Scan KTP</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-akun" role="tabpanel" aria-labelledby="pills-akun-tab">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="inputEmail4">Email</label>
                                                    <div class="input-group">
                                                        <input type="email" class="form-control" id="inputEmail5">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-mail fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="inputPassword4">Password</label>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="inputPassword5">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputPassword4">Konfirmasi Password</label>
                                                    <div class="input-group">
                                                        <input type="password" class="form-control" id="inputPassword5">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- /. col -->
                </div> <!-- /. end-section -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->