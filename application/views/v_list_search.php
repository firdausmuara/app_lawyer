<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h5>Hasil Pencarian : <?php echo $total_rows; ?></h5>
                <div class="list-group">
                    <!-- LAWYER AKSES -->
                    <?php if ($this->session->userdata('akses') == '2') : ?>

                        <?php if (empty($data)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <span class="fe fe-alert-circle fe-16 mr-2"></span> Data Tidak Ditemukan!
                            </div>
                        <?php } ?>

                        <?php foreach ($data as $row) { ?>
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start mt-3">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1"><?php echo $row->case_name ?></h5>
                                    <small>3 days ago</small>
                                </div>
                                <p class="mb-1">Tipe Kasus : <?php echo $row->case_type ?></p>
                                <p class="mb-1">Nama Client : <?php echo $row->client_name ?></p>
                                <small>dibuat oleh <?php echo $row->lawyer_name ?></small>
                            </a>
                        <?php } ?>

                        <!-- ADMIN AKSES -->
                    <?php elseif ($this->session->userdata('akses') == '1') : ?>

                        <?php if (empty($all)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <span class="fe fe-alert-circle fe-16 mr-2"></span> Data Tidak Ditemukan!
                            </div>
                        <?php } ?>

                        <?php foreach ($all as $row) { ?>
                            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start mt-3">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1"><?php echo $row->case_name ?></h5>
                                    <small>3 days ago</small>
                                </div>
                                <p class="mb-1">Tipe Kasus : <?php echo $row->case_type ?></p>
                                <p class="mb-1">Nama Client : <?php echo $row->client_name ?></p>
                                <small>dibuat oleh <?php echo $row->lawyer_name ?></small>
                            </a>
                        <?php } ?>

                    <?php endif; ?>
                </div>

                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->
    <!-- Modal Shortcut -->