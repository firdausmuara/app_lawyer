<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="page-title">Kasus Baru</h2>
                <div class="card my-4">
                    <div class="card-header">
                        <strong>Langkah - langkah</strong>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Isi Form</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Pilih Menu</a>
                                </li>
                            </ul>
                            <div class="tab-content mb-1" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <form action="" class="needs-validation" novalidate>
                                        <div class="form-group">
                                            <label for="inputCity">Pilih Client *</label>
                                            <select id="inputState5" class="form-control" required>
                                                <option selected value="">Choose...</option>

                                                <?php foreach ($data as $row) { ?>
                                                    <option value="<?php echo $row->id_client ?>"><?php echo $row->client_name ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="jenisKasus">Pilih Jenis Kasus *</label>
                                            <select id="jenisKasus" class="form-control" required>
                                                <option selected value="">Choose...</option>
                                                <option value="pidana">Pidana</option>
                                                <option value="perdata">Perdata</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="namaKasus">Nama Kasus *</label>
                                            <input type="text" id="namaKasus" class="form-control" required>
                                        </div>
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                    <div class="row align-items-center">
                                        <div class="col-4 text-center">
                                            <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                                                <div class="squircle bg-success justify-content-center">
                                                    <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                                                </div>
                                            </a>
                                            <p>L. Kronologis</p>
                                        </div>
                                        <div class="col-4 text-center">
                                            <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                                                <div class="squircle bg-primary justify-content-center">
                                                    <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                                                </div>
                                            </a>
                                            <p>Surat Kuasa</p>
                                        </div>
                                        <div class="col-4 text-center">
                                            <a href="#" data-toggle="modal" data-target="#varyModal" data-whatever="@mdo">
                                                <div class="squircle bg-warning justify-content-center">
                                                    <i class="fe fe-book-open fe-32 align-self-center text-white"></i>
                                                </div>
                                            </a>
                                            <p>Gugatan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->