<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="mb-2 page-title">List Kasus</h2>
                <div class="row my-4">
                    <!-- Small table -->
                    <div class="col-md-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <?php if ($this->session->userdata('akses') == '2') : ?>
                                    <!-- table by lawyer_id -->
                                    <table class="table datatables" id="dataTable-1">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Kasus</th>
                                                <th>Tipe Kasus</th>
                                                <th>Tanggal Dibuat</th>
                                                <th>Tanggal Diubah</th>
                                                <th>Status Aktif</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $i = 1;
                                            foreach ($data as $row) {
                                            ?>

                                                <tr>
                                                    <td><?php echo $i++ ?></td>
                                                    <td><?php echo $row->case_name ?></td>
                                                    <td><?php echo $row->case_type ?></td>
                                                    <td><?php echo $row->created_at ?></td>
                                                    <td><?php echo $row->edited_at ?></td>
                                                    <td>
                                                        <?php
                                                        if ($row->status == '1') {
                                                            echo "Aktif";
                                                        } else {
                                                            echo "Tidak Aktif";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="#"><i class="fe fe-edit"></i> Edit</a>
                                                            <a class="dropdown-item" href="#"><i class="fe fe-trash"></i> Hapus</a>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>

                                <?php elseif ($this->session->userdata('akses') == '1') : ?>
                                    <!-- table all data-->
                                    <table class="table datatables" id="dataTable-1">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Kasus</th>
                                                <th>Tipe Kasus</th>
                                                <th>Tanggal Dibuat</th>
                                                <th>Tanggal Diubah</th>
                                                <th>Status Aktif</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $i = 1;
                                            foreach ($all as $row) {
                                            ?>

                                                <tr>
                                                    <td><?php echo $i++ ?></td>
                                                    <td><?php echo $row->case_name ?></td>
                                                    <td><?php echo $row->case_type ?></td>
                                                    <td><?php echo $row->created_at ?></td>
                                                    <td><?php echo $row->edited_at ?></td>
                                                    <td>
                                                        <?php
                                                        if ($row->status == '1') {
                                                            echo "Aktif";
                                                        } else {
                                                            echo "Tidak Aktif";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="#"><i class="fe fe-edit"></i> Edit</a>
                                                            <a class="dropdown-item" href="#"><i class="fe fe-trash"></i> Hapus</a>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>

                                <?php endif; ?>
                            </div>
                        </div>
                    </div> <!-- simple table -->
                </div> <!-- end section -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->