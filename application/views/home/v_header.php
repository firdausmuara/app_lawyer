<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>App Lawyer</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="<?php echo base_url() ?>tinydash_assets/assets/images/logo.svg" rel="icon">
    <link href="<?php echo base_url() ?>tinydash_assets/assets/images/logo.svg" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="<?php echo base_url() ?>knight_assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>knight_assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>knight_assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>knight_assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>knight_assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>knight_assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="<?php echo base_url() ?>knight_assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: Knight - v2.2.1
  * Template URL: https://bootstrapmade.com/knight-free-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container">
            <a href="<?php echo base_url() ?>c_home" class="hero-logo" data-aos="zoom-in"><img src="<?php echo base_url() ?>knight_assets/img/hero-logo.png" alt=""></a>
            <h1 data-aos="zoom-in">Selamat Datang di Aplikasi Lawyer</h1>
            <a data-aos="fade-up" href="<?php echo base_url() ?>c_login" class="btn-get-started scrollto">Login</a>
        </div>
    </section><!-- End Hero -->

    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center">
        <div class="container">

            <!-- The main logo is shown in mobile version only. The centered nav-logo in nav menu is displayed in desktop view  -->
            <div class="logo d-block d-lg-none">
                <a href="<?php echo base_url() ?>c_home"><img src="<?php echo base_url() ?>knight_assets/img/logo.png" alt="" class="img-fluid"></a>
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul class="nav-inner">
                    <li class="active"><a href="<?php echo base_url() ?>c_home">Beranda</a></li>
                    <li><a href="#about">Tentang</a></li>
                    <li><a href="#services">Layanan</a></li>

                    <li class="nav-logo"><a href="<?php echo base_url() ?>c_home"><img src="<?php echo base_url() ?>knight_assets/img/logo.png" alt="" class="img-fluid"></a></li>

                    &nbsp;&nbsp;&nbsp;&nbsp;<li><a href="#portfolio">Kasus</a></li>
                    &nbsp;&nbsp;&nbsp;&nbsp;<li><a href="#pricing">Paket</a></li>
                    &nbsp;&nbsp;&nbsp;&nbsp;<li><a href="#contact">Kontak</a></li>

                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->