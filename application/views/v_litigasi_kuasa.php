      <main role="main" class="main-content">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="col-12">
              <h2 class="mb-2 page-title">Surat Kuasa</h2>
              <!-- The Froala editor gets initialized inside of this DIV element -->
              <div id="Kuasa" class="editor froala-editor">
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;">
                  <strong>
                    <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">SURAT KUASA KHUSUS</span>
                  </strong>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Yang bertandatangan di bawah ini:</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;margin-left:70.9pt;text-align:justify;text-indent:-70.9pt;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Nama&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: <strong><<--NAMA CLIENT-->></strong></span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;margin-left:70.9pt;text-align:justify;text-indent:-70.9pt;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Alamat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: <<--ALAMAT CLIENT-->></span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Selanjutnya disebut sebagai “<strong>Pemberi Kuasa</strong>”.</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <ol style="list-style-type: decimal;margin-left:0in;">
                  <li><strong><span style="font-family:&quot;Book Antiqua&quot;,serif;font-size:16px;"><<--NAMA LAWYER-->></span></strong></li>
                </ol>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Advokat pada kantor <strong><<--NAMA KANTOR LAWYER-->></strong>, yang beralamat di <strong><<--ALAMAT LAWYER-->></strong></span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Selanjutnya disebut sebagai “<strong>Penerima Kuasa</strong>”.</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">-----------------------------------------------------<strong>KHUSUS</strong>----------------------------------------------</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Untuk dan atas nama Pemberi Kuasa baik sendiri-sendiri maupun bersama - sama mewakili dan /atau mendampingi Pemberi Kuasa untuk menghadapi permasahalan hukum Perdata, dimana dalam hal ini Pemberi Kuasa telah mengalami permasalahan hukum yang dilakukan oleh <strong><<--COUNTER PART-->></strong> yang beralamat di <strong><<--ALAMAT COUNTER PART-->></strong>.</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:justify;">
                  <span style="font-family:&quot;Book Antiqua&quot;,serif;">Berkenaan dengan itu, Penerima Kuasa diberikan kewenangan penuh untuk melaksanakan segala hal-hal berkenaan dengan kepentingan hukum Pemberi Kuasa dalam arti seluas-luasnya termasuk namun tidak terbatas pada:</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:justify;">
                  <span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
                  <tbody>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">mempersiapkan dan mengajukan surat surat-surat atau dokumen-dokumen relevan lainnya untuk kepentingan Pemberi Kuasa terkait permasalahan hukum sebagaimana dijelaskan di atas;</span>
                            </li>
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">mengajukan dan mendaftarkan Gugatan di kantor kepaniteraan Pengadilan Negeri Jakarta Utara;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">menghadap dan menghadiri setiap persidangan termasuk proses mediasi dalam perkara tersebut bersama – sama dengan Pemberi Kuasa di Pengadilan Negeri Jakarta Utara;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">menghadap para pejabat pengadilan termasuk hakim, panitera, juru sita, dan pejabat-pejabat lainnya sepanjang yang diperbolehkan oleh hukum;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">menghadap para pejabat negeri, baik sipil maupun militer dan/atau perseorangan atau pribadi lainnya yang ada hubungannya dengan perkara ini sepanjang yang diperbolehkan oleh hukum;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">mempersiapkan alat bukti, baik berupa dokumen tertulis maupun saksi-saksi dan/atau ahli-ahli untuk kepentingan Pemberi Kuasa;</span>
                            </li>
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">membuat dan mengirimkan surat-surat atau dokumen-dokumen yang dianggap perlu kepada instansi pemerintah, Pengadilan atau instansi yudikatif lainnya serta terhadap pihak-pihak yang berkaitan dengan perkara ini;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">mengadakan dan/atau menghadiri segala pertemuan, diskusi, perundingan, negosiasi bersama – sama dengan Pemberi Kuasa baik dengan Tergugat dan bersama – sama / atau kuasa hukumnnya maupun dengan pihak-pihak lain yang ada kaitannya dengan masalah hutang tersebut baik di dalam maupun di luar semua persidangan di Pengadilan Negeri Jakarta Utara;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">mengadakan perdamaian bersama – sama dengan Pemberi Kuasa, termasuk mempersiapkan dan membuat usulan perdamaian dan / atau perjanjian perdamaian yang dianggap baik dan diperbolehkan oleh hukum yang berlaku;</span>
                            </li>
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">meminta segala salinan putusan dan/atau penetapan yang dijatuhkan atau dikeluarkan oleh Pengadilan Negeri Jakarta Utara dan pengadilan-pengadilan lainnya yang termasuk yurisdiksi perkara tersebut; serta salinan atau petikan dari semua surat sehubungan dengan perkara tersebut pada setiap tingkat pemeriksaan;</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 6.4in;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="100%">
                        <div style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                          <ul style="margin-bottom:0in;list-style-type: disc;margin-left:-0.25in;">
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">Memberikan keterangan-keterangan yang menurut hukum harus atau boleh dilakukan oleh seorang advokat atau kuasa.</span>
                            </li>
                            <li style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;">
                              <span style="font-family:&quot;Book Antiqua&quot;,serif;">Melakukan dan atau mengajukan permohonan</span><em><span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;aanmaning</span></em><span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;dan/atau eksekusi untuk menjalankan isi Putusan Pengadilan yang berkaitan dengan kepentingan hukum Pemberi Kuasa</span>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;margin-left:21.3pt;text-align:justify;">
                  <span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Kuasa</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">ini</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">diberikan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">dengan hak retensi dan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">hak</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">subsitusi</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">baik</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">sebagian</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">maupun</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">seluruhnya.</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Pemberi</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">Kuasa</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">meratifikasi</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">segala</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">tindakan/perbuatan-perbuatan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">h</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">u</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">kum</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">dan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">tindakan-tindakan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">lainnya yang dilakukan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">oleh Penerima Kuasa yang dianggap</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">baik</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">oleh Penerima Kuasa untuk</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">melaksanakan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">kekuasaan yang diberikan</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">dalam</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">surat</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">kuasa</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span><span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">ini.</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:justify;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:10.0pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:right;">
                  <span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:10.0pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;text-align:center;">
                  <span style="font-family:&quot;Book Antiqua&quot;,serif;">Jakarta, 1</span><span style="font-family:&quot;Book Antiqua&quot;,serif;">2 Agustus</span><span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;20</span><span style="font-family:&quot;Book Antiqua&quot;,serif;">20</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
                  <tbody>
                    <tr>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;"><strong>
                            <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">PEMBERI KUASA</span></strong></p>
                      </td>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;"><strong>
                            <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">PENERIMA KUASA</span></strong></p>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;height: 103pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;">
                          <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                        </p>
                      </td>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;height: 103pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;">
                          <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;"><strong><u>
                              <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">NAMA CLIENT</span></u></strong></p>
                      </td>
                      <td style="width: 231.05pt;padding: 0in 5.4pt;vertical-align: top;" valign="top" width="50%">
                        <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;text-align:center;"><strong><u>
                              <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">NAMA LAWYER</span></strong></u></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:&quot;Calibri&quot;,sans-serif;">
                  <span style="font-size:16px;font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span>
                </p>
                <p style="margin:0in;margin-bottom:.0001pt;font-size:16px;font-family:&quot;Times New Roman&quot;,serif;"><strong>
                    <span style="font-family:&quot;Book Antiqua&quot;,serif;">&nbsp;</span></strong></p>
              </div>
            </div>
          </div> <!-- .row -->
        </div> <!-- .container-fluid -->
        <!-- Modal Shortcut -->