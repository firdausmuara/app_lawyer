<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row align-items-center mb-2">
                    <div class="col">
                        <h2 class="h5 page-title">Welcome!</h2>
                    </div>
                    <div class="col-auto">
                        <form class="form-inline">
                            <div class="form-group d-none d-lg-inline">
                                <label for="reportrange" class="sr-only">Date Ranges</label>
                                <div id="reportrange" class="px-2 py-2 text-muted">
                                    <span class="small"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-sm"><span class="fe fe-refresh-ccw fe-16 text-muted"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card shadow my-4">
                    <div class="card-body">
                        <div class="row align-items-center my-4">
                            <div class="col-md-12">
                                <div class="mx-4">
                                    <?php if ($this->session->userdata('akses') == '1') : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Super Admin</strong><br />
                                    <?php elseif ($this->session->userdata('akses') == '2') : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Lawyer</strong><br />
                                    <?php else : ?>
                                        <strong class="mb-0 text-uppercase text-muted">Client</strong><br />
                                    <?php endif; ?>
                                    <h3><?php echo $this->session->userdata('ses_nama'); ?></h3>
                                    <p class="text-muted"><?php echo $this->session->userdata('ses_address'); ?></p>
                                </div>
                            </div>
                        </div> <!-- end section -->
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div>
            <!-- The Froala editor gets initialized inside of this DIV element -->
            <!-- <div id="froala-editor"></div> -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->
    <!-- Modal Shortcut -->