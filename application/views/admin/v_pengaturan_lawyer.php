<main role="main" class="main-content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <h2 class="mb-2 page-title">Pengaturan Lawyer</h2>
        <div class="row my-4">
          <!-- Small table -->
          <div class="col-md-12">
            <div class="card shadow">
              <div class="card-body">
                <div class="dropdown float-right">
                  <a href="<?php echo base_url(); ?>c_lawyer/tambah" class="btn btn-primary float-right ml-3" type="button">Tambah Data +</a>
                  <button class="btn btn-secondary dropdown-toggle" type="button" id="actionMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Aksi </button>
                  <div class="dropdown-menu" aria-labelledby="actionMenuButton">
                    <a class="dropdown-item" href="#">Export</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
                <!-- table -->
                <table class="table datatables" id="dataTable-1">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama Lawyer</th>
                      <th>No KTP Lawyer</th>
                      <th>Email Lawyer</th>
                      <th>Nomor Lawyer</th>
                      <th>Alamat</th>
                      <th>Password</th>
                      <th>Paket Tipe</th>
                      <th>Status Aktif</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $i = 1;
                    foreach ($data as $row) {
                    ?>

                      <tr>
                        <td><?php echo $i++ ?></td>
                        <td><?php echo $row->full_name ?></td>
                        <td><?php echo $row->ktp_number ?></td>
                        <td><?php echo $row->email ?></td>
                        <td><?php echo $row->phone_number ?></td>
                        <td><?php echo $row->full_address ?></td>
                        <td><?php echo $row->password ?></td>
                        <td>
                          <?php if ($row->id_package == 1) : ?>
                            <span class="badge badge-pill badge-primary"><?php echo $row->paket_tipe ?></span>
                          <?php elseif ($row->id_package == 2) : ?>
                            <span class="badge badge-pill badge-warning"><?php echo $row->paket_tipe ?></span>
                          <?php elseif ($row->id_package == 3) : ?>
                            <span class="badge badge-pill badge-success"><?php echo $row->paket_tipe ?></span>
                          <?php endif; ?>
                        </td>
                        <td>
                          <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="customSwitch1" <?php echo ($row->status == 1 ? 'checked' : ''); ?>>
                            <label class="custom-control-label" for="customSwitch1" id="labelSwitch"></label>
                          </div>
                        </td>
                        <td>
                          <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="text-muted sr-only">Aksi</span>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#"><i class="fe fe-edit"></i> Edit</a>
                            <a class="dropdown-item" href="#"><i class="fe fe-trash"></i> Hapus</a>
                          </div>
                        </td>
                      </tr>

                    <?php
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div> <!-- simple table -->
        </div> <!-- end section -->
      </div> <!-- .col-12 -->
    </div> <!-- .row -->
  </div> <!-- .container-fluid -->