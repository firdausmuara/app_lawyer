<main role="main" class="main-content">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <h2 class="mb-2 page-title">Client Baru</h2>
        <div class="row my-4">
          <!-- Small table -->
          <div class="col-md-12">
            <div class="card shadow">
              <div class="card-body">
                <?php if ($this->session->userdata('akses') == '2') : ?>
                  <!-- table by lawyer_id -->
                  <table class="table" id="dtTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama Client</th>
                        <th>Email Client</th>
                        <th>Nomor Client</th>
                        <th>Alamat</th>
                        <th>Password</th>
                        <th>Status Aktif</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody id="list_client_by_lawyer">

                      <!-- <?php
                            $i = 1;
                            foreach ($data as $row) {
                            ?>

                            <tr>
                              <td><?php echo $i++ ?></td>
                              <td><?php echo $row->client_name ?></td>
                              <td><?php echo $row->client_email ?></td>
                              <td><?php echo $row->client_phone ?></td>
                              <td><?php echo $row->client_address ?></td>
                              <td><?php echo $row->password ?></td>
                              <td><?php echo $row->status ?></td>
                              <td><button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <span class="text-muted sr-only">Action</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                  <a class="dropdown-item" href="#"><i class="fe fe-edit"></i> Edit</a>
                                  <a class="dropdown-item" href="#"><i class="fe fe-trash"></i> Hapus</a>
                                </div>
                              </td>
                            </tr>

                          <?php
                            }
                          ?> -->

                    </tbody>
                  </table>

                <?php elseif ($this->session->userdata('akses') == '1') : ?>
                  <!-- table all data-->
                  <table class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama Client</th>
                        <th>Email Client</th>
                        <th>Nomor Client</th>
                        <th>Alamat</th>
                        <th>Password</th>
                        <th>Nama Lawyer</th>
                        <th>Status Aktif</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>

                      <!-- <?php
                            $i = 1;
                            foreach ($all as $row) {
                            ?>

                        <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $row->client_name ?></td>
                          <td><?php echo $row->client_email ?></td>
                          <td><?php echo $row->client_phone ?></td>
                          <td><?php echo $row->client_address ?></td>
                          <td><?php echo $row->client_password ?></td>
                          <td><?php echo $row->full_name ?></td>
                          <td>
                            <?php
                              if ($row->status == '1') {
                                echo "Aktif";
                              } else {
                                echo "Tidak Aktif";
                              }
                            ?>
                          </td>
                          <td>
                            <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="text-muted sr-only">Aksi</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a class="dropdown-item" href="#"><i class="fe fe-edit"></i> Edit</a>
                              <a class="dropdown-item" href="#"><i class="fe fe-trash"></i> Hapus</a>
                            </div>
                          </td>
                        </tr>

                      <?php
                            }
                      ?> -->

                    </tbody>
                  </table>

                <?php endif; ?>
              </div>
            </div>
          </div> <!-- simple table -->
        </div> <!-- end section -->
      </div> <!-- .col-12 -->
    </div> <!-- .row -->
  </div> <!-- .container-fluid -->


  <script>
    $(document).ready(function() {
      getDataList();
      setTimeout(() => {
        $("[data-toggle='tooltip']").tooltip()
        $('#dtTable').DataTable()
      }, 500);
    })

    function getDataList() {
      var id_lawyer = <?php echo $this->session->userdata('ses_id'); ?>
      // console.log(id_lawyer);
      $.ajax({
        url: 'https://be-applawyer.demodantest.in/api/client/lawyer',
        method: 'GET',
        data: {
          id_lawyer: id_lawyer,
          status: '2'
        },
        dataType: 'json',
        success: function(data) {
          $("#list_client_by_lawyer").html('');
          var html = ' ';
          console.log('data by lawyer', data);
          var no = 1;
          for (var i in data) {
            const law = data[i];
            //  console.log(law);

            html += `<tr>
                        <td>${no}</td>
                        <td>
                          <a href="<?php echo base_url() ?>c_client/detail_client/${law.id_client}">${law.client_name}</a>
                        </td>
                        <td>${law.client_email}</td>
                        <td>${law.client_phone}</td>
                        <td>${law.client_address}</td>
                        <td>${law.client_password}</td>
                        <td>
                          <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="status">
                            <label class="custom-control-label" for="status" id="labelSwitch"></label>
                          </div>
                        </td>
                        <td>
                          <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="text-muted sr-only">Aksi</span>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right">
                            <a href="<?= base_url(); ?>c_admin/chat" class="dropdown-item"><i class="fe fe-message-circle"></i> Chat</a>
                            <a href="<?= base_url(); ?>c_client/tambah?aksi=edit&id_client=${law.id_client}" class="dropdown-item"><i class="fe fe-edit"></i> Edit</a>
                            <button class="dropdown-item" onclick="confirm_delete(${law.id_client})"><i class="fe fe-trash"></i> Hapus</button>
                          </div>
                        </td>
                      </tr>`;
            no++;
          }
          $("#list_client_by_lawyer").append(html);
        }
      });
    }

    function confirm_delete(id_client) {

      swal({
        title: "Anda Yakin ingin Hapus Client ini?",
        text: "Anda Tidak akan Melihat Data ini lagi!",
        icon: "warning",
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: {
          cancel: true,
          confirm: true,
        }
      }).then((isConfirmed) => {
        if (isConfirmed) {
          $.ajax({
            url: 'https://be-applawyer.demodantest.in/api/client/' + id_client,
            method: 'POST',
            data: {
              id_client: id_client
            },
            dataType: 'json',
            success: function(data) {
              swal("Berhasil", "Client Berhasil Dihapus", "success");
              location.reload();
            }
          });
        } else {
          swal("Gagal", "Client Gagal Dihapus", "warning");
        }
      })

    }
  </script>