    <main role="main" class="main-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <?php if ($this->input->get('aksi') == 'tambah') : ?>
                <div class="col-12">
                    <h2 class="page-title">Tambah Data</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Perorangan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-akun-tab" data-toggle="pill" href="#pills-akun" role="tab" aria-controls="pills-akun" aria-selected="true">Badan Hukum</a>
                                        </li>
                                    </ul>
                                    
                                    <form action="javascript:void(0);" method="POST" id="formClient" enctype="multipart/form-data" >
                                        <div class="tab-content mb-1" id="pills-tabContent">
                                            <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                <div class="form-group">
                                                    <label for="inputNama">Nama Lengkap</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="name_client" placeholder="Nama Lengkap" name="name_client">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNomor">Email</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="email_client" placeholder="example@example.com" name="email_client">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNomor">Nomor KTP</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="ktp_client" placeholder="312xxxxx" name="ktp_client">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputNomor">Nomor Handphone</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="phone_client" placeholder="08xxxxxxx" name="phone_client">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress">Alamat Lengkap</label>
                                                    <textarea name="address_client" class="form-control" id="address_client" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455"></textarea>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Provinsi</label>
                                                        <select id="province" class="form-control" name="province">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Kota</label>
                                                        <select id="kab_kota" class="form-control" name="kab_kota">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputState">Kecamatan</label>
                                                        <select id="kecamatan" class="form-control" name="kecamatan">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputZip">Kelurahan</label>
                                                        <select id="kelurahan" class="form-control" name="kelurahan">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label for="customFile">Foto Scan KTP</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="client_ktp_scan" name="client_ktp_scan">
                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="pills-akun" role="tabpanel" aria-labelledby="pills-akun-tab">

                                                <div class="form-group">
                                                    <label for="inputNama">Nama Badan Hukum</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="nama_bakum" placeholder="Nama Badan Hukum" name="nama_bakum">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputAddress">Alamat Lengkap</label>
                                                    <textarea name="client_address" class="form-control" id="alamat_bakum" name="alamat_bakum" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455"></textarea>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Provinsi</label>
                                                        <select id="provinsi_bakum" class="form-control" name="provinsi_bakum">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Kota</label>
                                                        <select id="kab_kota_bakum" class="form-control" name="kab_kota_bakum">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputState">Kecamatan</label>
                                                        <select id="kecamatan_bakum" class="form-control" name="kecamatan_bakum">
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputZip">Kelurahan</label>
                                                        <select id="kelurahan_bakum" class="form-control" name="kelurahan_bakum">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Nomor Akta</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="no_akta_bakum" name="no_akta_bakum" placeholder="Nomor Akta">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Tanggal Akta</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control drgpicker" id="tgl_akta_bakum" name="tgl_akta_bakum" placeholder="Tanggal Akta">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">Nama Notaris</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="nama_notaris_bakum" name="nama_notaris_bakum" placeholder="Nama Notaris">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <label for="inputCity">SK Menkumham</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="sk_menkumham_bakum" name="sk_menkumham_bakum" placeholder="SK Menkumham">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="inputCity">Upload Dokumen</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="dokumen_bakum" name="dokumen_bakum">
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Password</label>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" id="inputPassword5">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="inputPassword4">Konfirmasi Password</label>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" id="inputPassword5">
                                                            <div class="input-group-append">
                                                                <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <button type="submit" class="btn btn-primary">Tambah</button>
                                            </div>
                                            <!-- <a href="<?= base_url(); ?>c_client/new_client" type="submit" class="btn btn-primary">Tambah</a> -->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> <!-- /. col -->
                    </div> <!-- /. end-section -->
                </div><!-- .col-12 -->

                <?php elseif ($this->input->get('aksi') == 'edit') : ?>
                    <div class="col-12">
                        <h2 class="page-title">Edit Data</h2>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card shadow">
                                    <div class="card-body clientForm">
                                            
                                    </div>
                                </div>
                            </div> <!-- /. col -->
                        </div> <!-- /. end-section -->
                    </div><!-- .col-12 -->
                <?php endif; ?>
            </div> <!-- .row -->
        </div> <!-- .container-fluid -->

        <script>
            // var idbakum = ''
            $(document).ready(function() {
                getDataList();
                getProvinsi();
                changeKabupaten();
                changeKecamatan();
                changeKelurahan();
            })
            function getDataList() {
                var id_client = <?= $this->input->get('id_client'); ?>
                // console.log(id_client);
                $.ajax({
                    url: 'https://be-applawyer.demodantest.in/api/client/' + id_client,
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {

                    // idbakum = data.client_kabupaten;
                    $(".clientForm").html(`<ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Perorangan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-akun-tab" data-toggle="pill" href="#pills-akun" role="tab" aria-controls="pills-akun" aria-selected="true">Badan Hukum</a>
                                    </li>
                                </ul>
                                    <div class="tab-content mb-1" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="form-group">
                                                <label for="inputNama">Nama Lengkap</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="id_client" placeholder="Nama Lengkap" name="id_client" value="${data.id_client}" hidden>
                                                    <input type="text" class="form-control" id="client_name" placeholder="Nama Lengkap" name="client_name" value="${data.client_name}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNomor">Email</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="client_email" placeholder="example@example.com" name="client_email" value="${data.client_email}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNomor">Nomor KTP</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="client_ktp" placeholder="312xxxxx" name="client_ktp" value="${data.client_ktp}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputNomor">Nomor Handphone</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="client_phone" placeholder="08xxxxxxx" name="client_phone" value="${data.client_phone}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputAddress">Alamat Lengkap</label>
                                                <textarea name="client_address" class="form-control" id="client_address" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455">${data.client_address}</textarea>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Provinsi</label>
                                                    <select id="province" class="form-control" name="province">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Kota</label>
                                                    <select id="kab_kota" class="form-control" name="kab_kota">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputState">Kecamatan</label>
                                                    <select id="kecamatan" class="form-control" name="kecamatan">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputZip">Kelurahan</label>
                                                    <select id="kelurahan" class="form-control" name="kelurahan">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="customFile">Foto Scan KTP</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="client_ktp_scan" name="client_ktp_scan">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-akun" role="tabpanel" aria-labelledby="pills-akun-tab">
                                            <div class="form-group">
                                                <label for="inputNama">Nama Badan Hukum</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="id_bakum" placeholder="Nama Lengkap" name="id_bakum" value="${data.id_bakum}" hidden>
                                                    <input type="text" class="form-control" id="nama_bakum" placeholder="Nama Badan Hukum" name="nama_bakum" value="${data.nama_bakum}">
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputAddress">Alamat Lengkap</label>
                                                <textarea class="form-control" id="alamat_bakum" name="alamat_bakum" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455">${data.alamat_bakum}</textarea>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Provinsi</label>
                                                    <select id="provinsi_bakum" class="form-control" name="provinsi_bakum" value="">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Kota</label>
                                                    <select id="kab_kota_bakum" class="form-control" name="kab_kota_bakum" value="">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputState">Kecamatan</label>
                                                    <select id="kecamatan_bakum" class="form-control" name="kecamatan_bakum" value="">
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputZip">Kelurahan</label>
                                                    <select id="kelurahan_bakum" class="form-control" name="kelurahan_bakum" value="">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Nomor Akta</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="no_akta_bakum" name="no_akta_bakum" placeholder="Nomor Akta" value="${data.no_akta_bakum}">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Tanggal Akta</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="tgl_akta_bakum" name="tgl_akta_bakum" placeholder="Tanggal Akta" value="${data.tgl_akta_bakum}">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">Nama Notaris</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="nama_notaris_bakum" name="nama_notaris_bakum" placeholder="Nama Notaris" value="${data.nama_notaris_bakum}">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="inputCity">SK Menkumham</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="sk_menkumham_bakum" name="sk_menkumham_bakum" placeholder="SK Menkumham" value="${data.sk_menkumham_bakum}">
                                                        <div class="input-group-append">
                                                            <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="inputCity">Upload Dokumen</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="dokumen_bakum" name="dokumen_bakum" value="">
                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <button onclick="submitClient()" id="btnSubmit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>`);
                        getProvinsi(data.client_provinsi, data.provinsi_bakum);
                        getKabupaten(data.client_provinsi, data.client_kabupaten, data.kab_kota_bakum);
                        getKecamatan(data.client_kabupaten, data.client_kecamatan, data.kecamatan_bakum);
                        getKelurahan(data.client_kecamatan, data.client_kelurahan, data.kelurahan_bakum);
                    }
                });
            }

            
            // Tambah Data
            $("#formClient").submit(function(event) {
                // PRIBADI
                    var client_name = $("#name_client").val();
                    var client_email = $("#email_client").val();
                    var client_ktp = $("#ktp_client").val();
                    var client_phone = $("#phone_client").val();
                    var client_address = $("#address_client").val();
                    var province = $("#province").val();
                    var kab_kota = $("#kab_kota").val();
                    var kecamatan = $("#kecamatan").val();
                    var kelurahan = $("#kelurahan").val();
                    var myfiles = document.getElementById("client_ktp_scan");
                    var client_ktp_scan = myfiles.files;
                    var lawyer_id = <?= $this->session->userdata('ses_id') ?>;
                    var date = Date.now();
                    var password = 'default123';
                    var status = '2';
                    var avatar = 'default.jpg';

                var dok = new FormData();
                // PRIBADI
                    dok.append('client_name', client_name)
                    dok.append('client_email', client_email)
                    dok.append('client_ktp', client_ktp)
                    dok.append('client_phone', client_phone)
                    dok.append('client_address', client_address)
                    dok.append('province', province)
                    dok.append('kab_kota', kab_kota)
                    dok.append('kecamatan', kecamatan)
                    dok.append('kelurahan', kelurahan)
                    // dok.append('client_ktp_scan', client_ktp_scan)
                    dok.append('lawyer_id', lawyer_id)
                    dok.append('created_at', date)
                    dok.append('edited_at', date)
                    dok.append('password', password)
                    dok.append('status', status)
                    dok.append('avatar', avatar)

                    for (i = 0; i < client_ktp_scan.length; i++) {
                        dok.append('client_ktp_scan[]', client_ktp_scan[i]);
                    }

                for (var pair of dok.entries()) {
                    console.log(pair[0]+ ', ' + pair[1]); 
                }
                    
                $.ajax({
                    url: "https://be-applawyer.demodantest.in/api/client",
                    method: 'POST',
                    data: dok,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                    },
                    success: function (dt) {
                        console.log('sukses post pribadi',dt)
                        // BADAN HUKUM
                            var id_client = dt.id_client;
                            var nama_bakum = $("#nama_bakum").val();
                            var alamat_bakum = $("#alamat_bakum").val();
                            var provinsi_bakum = $("#provinsi_bakum").val();
                            var kab_kota_bakum = $("#kab_kota_bakum").val();
                            var kecamatan_bakum = $("#kecamatan_bakum").val();
                            var kelurahan_bakum = $("#kelurahan_bakum").val();
                            var no_akta_bakum = $("#no_akta_bakum").val();
                            var tgl_akta_bakum = $("#tgl_akta_bakum").val();
                            var nama_notaris_bakum = $("#nama_notaris_bakum").val();
                            var sk_menkumham_bakum = $("#sk_menkumham_bakum").val();
                            var dokumen_bakum = 'scan.pdf';

                        var bakum = new FormData();
                        // BADAN HUKUM
                            bakum.append('id_client', id_client)
                            bakum.append('nama_bakum', nama_bakum)
                            bakum.append('alamat_bakum', alamat_bakum)
                            bakum.append('provinsi_bakum', provinsi_bakum)
                            bakum.append('kab_kota_bakum', kab_kota_bakum)
                            bakum.append('kecamatan_bakum', kecamatan_bakum)
                            bakum.append('kelurahan_bakum', kelurahan_bakum)
                            bakum.append('no_akta_bakum', no_akta_bakum)
                            bakum.append('tgl_akta_bakum', tgl_akta_bakum)
                            bakum.append('nama_notaris_bakum', nama_notaris_bakum)
                            bakum.append('sk_menkumham_bakum', sk_menkumham_bakum)
                            bakum.append('dokumen_bakum', dokumen_bakum)

                        for (var pair of bakum.entries()) {
                            console.log(pair[0]+ ', ' + pair[1]); 
                        }
                        // return
                        $.ajax({
                            url: "https://be-applawyer.demodantest.in/api/client",
                            method: 'POST',
                            data: bakum,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            beforeSend: function () {
                            },
                            success: function (data) {
                                console.log('sukses post bakum',data)
                                window.location.href= 'new_client';
                                return
                            }
                        })
                    }
                })
            })

            function getProvinsi(by_id = null, by_id_bakum = null) {
                // console.log('Provinsi Client', by_id)
                $.ajax({
                    url: 'https://be-applawyer.demodantest.in/api/wilayah/provinsi/',
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $("#province").html('');
                        $("#provinsi_bakum").html('');
                        var option = '<option value="0">Pilih Provinsi</option>';
                        for(var i in data){
                            const op = data[i];
                            // console.log(op)
                            option += `<option value="${op.propinsi_kode}">${op.propinsi_nama}</option>`;
                        }
                        $("#province").append(option);
                        $("#provinsi_bakum").append(option);
                        $("#province").val(by_id)
                        $("#provinsi_bakum").val(by_id_bakum)
                    }
                });
            }
            

            function getKabupaten(provinsi_select_id = null, by_id = null, by_id_bakum = null) {
                $.ajax({
                    url: 'https://be-applawyer.demodantest.in/api/wilayah/kabupaten/' + provinsi_select_id,
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $("#kab_kota").html('');
                        var option = '<option value="0">Pilih Kabupaten</option>';
                        for(var i in data){
                            const op = data[i];
                            // console.log(op)
                            option += `<option value="${op.kab_kode}">${op.kab_nama}</option>`;
                        }
                        $("#kab_kota").append(option);

                        $("#kab_kota").val(by_id)
                    }
                });
            }

            function changeKabupaten() {
                $('#province').change(function() {
                    if (this.value == '0') {
                        $("#kab_kota").append(`<option value="0"></option>`);
                    } else {
                        var provinsi_id = this.value;
                        // console.log('provinsi id', provinsi_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kabupaten/' + provinsi_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kab_kota").html('');
                                var option = '<option value="0">Pilih Kabupaten</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kab_kode}">${op.kab_nama}</option>`;
                                }
                                $("#kab_kota").append(option);

                            $("#kab_kota").val(idbakum)
                            }
                        });
                    }
                })

                $('#provinsi_bakum').change(function() {
                    if (this.value == '0') {
                        $("#kab_kota_bakum").append(`<option value="0"></option>`);
                    } else {
                        var provinsi_id = this.value;
                        // console.log('provinsi id', provinsi_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kabupaten/' + provinsi_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kab_kota_bakum").html('');
                                var option = '<option value="0">Pilih Kabupaten</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kab_kode}">${op.kab_nama}</option>`;
                                }
                                $("#kab_kota_bakum").append(option);
                                $("#kab_kota_bakum").val(by_id)
                            }
                        });
                    }
                })
            }

            function getKecamatan(kabupaten_select_id = null, by_id = null, by_id_bakum = null) {
                $.ajax({
                    url: 'https://be-applawyer.demodantest.in/api/wilayah/kecamatan/' + kabupaten_select_id,
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $("#kecamatan").html('');
                        var option = '<option value="0">Pilih Kecamatan</option>';
                        for(var i in data){
                            const op = data[i];
                            // console.log(op)
                            option += `<option value="${op.kec_kode}">${op.kec_nama}</option>`;
                        }
                        $("#kecamatan").append(option);

                        $("#kecamatan").val(by_id)
                    }
                });
            }

            function changeKecamatan() {
                $('#kab_kota').change(function() {
                    if (this.value == '0') {
                        $("#kecamatan").append(`<option value="0"></option>`);
                    } else {
                        var kabupaten_id = this.value;
                        // console.log('kabupaten id', kabupaten_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kecamatan/' + kabupaten_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kecamatan").html('');
                                var option = '<option value="0">Pilih Kecamatan</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kec_kode}">${op.kec_nama}</option>`;
                                }
                                $("#kecamatan").append(option);
                            }
                        });
                    }
                })

                $('#kab_kota_bakum').change(function() {
                    if (this.value == '0') {
                        $("#kecamatan").append(`<option value="0"></option>`);
                    } else {
                        var kabupaten_id = this.value;
                        // console.log('kabupaten id', kabupaten_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kecamatan/' + kabupaten_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kecamatan_bakum").html('');
                                var option = '<option value="0">Pilih Kecamatan</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kec_kode}">${op.kec_nama}</option>`;
                                }
                                $("#kecamatan_bakum").append(option);
                            }
                        });
                    }
                })
            }

            function getKelurahan(kecamatan_select_id = null, by_id = null, by_id_bakum = null) {
                $.ajax({
                    url: 'https://be-applawyer.demodantest.in/api/wilayah/kelurahan/' + kecamatan_select_id,
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        $("#kelurahan").html('');
                        var option = '<option value="0">Pilih Kelurahan</option>';
                        for(var i in data){
                            const op = data[i];
                            // console.log(op)
                            option += `<option value="${op.kel_kode}">${op.kel_nama}</option>`;
                        }
                        $("#kelurahan").append(option);

                        $("#kelurahan").val("0" + by_id)
                    }
                });
            }

            function changeKelurahan() {
                $('#kecamatan').change(function() {
                    if (this.value == '0') {
                        $("#kelurahan").append(`<option value="0"></option>`);
                    } else {
                        var kecamatan_id = this.value;
                        // console.log('kecamatan id', kecamatan_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kelurahan/' + kecamatan_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kelurahan").html('');
                                var option = '<option value="0">Pilih Kelurahan</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kel_kode}">${op.kel_nama}</option>`;
                                }
                                $("#kelurahan").append(option);
                            }
                        });
                    }
                })

                $('#kecamatan_bakum').change(function() {
                    if (this.value == '0') {
                        $("#kelurahan").append(`<option value="0"></option>`);
                    } else {
                        var kecamatan_id = this.value;
                        // console.log('kecamatan id', kecamatan_id);
                        $.ajax({
                            url: 'https://be-applawyer.demodantest.in/api/wilayah/kelurahan/' + kecamatan_id,
                            method: 'GET',
                            dataType: 'json',
                            success: function(data) {
                                $("#kelurahan_bakum").html('');
                                var option = '<option value="0">Pilih Kelurahan</option>';
                                for(var i in data){
                                    const op = data[i];
                                    // console.log(op)
                                    option += `<option value="${op.kel_kode}">${op.kel_nama}</option>`;
                                }
                                $("#kelurahan_bakum").append(option);
                            }
                        });
                    }
                })
            }
        </script>