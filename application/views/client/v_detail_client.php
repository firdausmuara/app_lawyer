<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card my-4">
                    <div class="card-header">
                        <strong>Dokumen</strong>
                    </div>
                    <div class="card shadow">
                        <div class="card-body">
                            <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Lembar Kronologis</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-bukti-tab" data-toggle="pill" href="#pills-bukti" role="tab" aria-controls="pills-bukti" aria-selected="true">List Bukti</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-gugatan-tab" data-toggle="pill" href="#pills-gugatan" role="tab" aria-controls="pills-gugatan" aria-selected="true">Dokumen Gugatan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Legal Research</a>
                                </li>
                            </ul>
                            <div class="tab-content mb-1" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                </div>
                                <div class="tab-pane fade" id="pills-bukti" role="tabpanel" aria-labelledby="pills-bukti-tab">
                                    <table class="table datatables" id="dataTable-1">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama File</th>
                                                <th>Tanggal Bukti</th>
                                                <th width="500px">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php
                                            $i = 1;
                                            foreach ($data as $row) {
                                            ?>

                                                <tr>
                                                    <td><?php echo $i++ ?></td>
                                                    <td>
                                                        <a href="#"><?php echo $row->nama_file ?></a>
                                                    </td>
                                                    <td><?php echo $row->tanggal_bukti ?></td>
                                                    <td><?php echo $row->keterangan ?></td>
                                                </tr>

                                            <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="pills-gugatan" role="tabpanel" aria-labelledby="pills-gugatan-tab">
                                </div>
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->