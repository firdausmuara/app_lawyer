<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="mb-2 page-title">List Dasar-dasar Hukum</h2>
                <div class="row my-4">
                    <!-- Small table -->
                    <div class="col-md-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <!-- table -->
                                <table class="table datatables" id="dataTable-1">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nomor Pasal</th>
                                            <th>Ayat</th>
                                            <th>Isi</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($data as $row) {
                                        ?>

                                            <tr>
                                                <td><?php echo $i++ ?></td>
                                                <td><?php echo $row->pasal_no ?></td>
                                                <td><?php echo $row->ayat ?></td>
                                                <td>
                                                    <textarea id="" cols="30" rows="10"><?php echo $row->isi ?></textarea>
                                                </td>
                                            </tr>

                                        <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> <!-- simple table -->
                </div> <!-- end section -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->