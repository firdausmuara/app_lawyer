<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <!-- <h2 class="mb-2 page-title">Client Baru</h2> -->
                <div class="row my-4">
                    <div class="card shadow mb-4">
                        <div class="card-header">
                            <strong class="card-title">Chat</strong>
                            <!-- <span class="float-right"><i class="fe fe-message-circle mr-2"></i>4</span> -->
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center mb-4">
                                <div class="col-auto">
                                    <div class="avatar avatar-sm mb-3 mx-4">
                                        <img src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-3.jpg" alt="..." class="avatar-img rounded-circle">
                                    </div>
                                </div>
                                <div class="col">
                                    <strong>Hester Nissim</strong>
                                    <div class="mb-2">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</div>
                                    <div class="card mb-3 bg-light w-50">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-md-2 text-center">
                                                <img src="<?php echo base_url() ?>tinydash_assets/assets/products/p1.jpg" alt="..." class="img-fluid rounded m-1">
                                            </div>
                                            <div class="col-md-10">
                                                <div class="card-body py-0">
                                                    <p class="card-title mb-0">New screenshot-12.png</p>
                                                    <div class="card-text my-0 text-muted small"><span class="mr-2">1.2M</span><span class="mr-2">SVG</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <small class="text-muted">2020-04-21 08:48:18</small>
                                </div>
                                <div class="col-auto">
                                    <span class="circle circle-sm bg-light">
                                        <i class="fe fe-corner-down-left"></i>
                                    </span>
                                </div>
                            </div> <!-- .row-->
                            <div class="row align-items-center mb-4">
                                <div class="col-auto">
                                    <div class="avatar avatar-sm mb-3 mx-4">
                                        <img src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-4.jpg" alt="..." class="avatar-img rounded-circle">
                                    </div>
                                </div>
                                <div class="col">
                                    <strong>Kelley Sonya</strong>
                                    <div class="mb-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin luctus pretium. <br>Pellentesque porta massa ac nibh finibus iaculis. Maecenas vel interdum urna. Integer auctor ultrices faucibus. Aliquam consequat et ligula nec sodales.</div>
                                    <small class="text-muted">2020-04-21 12:01:22</small>
                                </div>
                                <div class="col-auto">
                                    <span class="circle circle-sm bg-light">
                                        <i class="fe fe-corner-down-left"></i>
                                    </span>
                                </div>
                            </div> <!-- .row-->
                            <hr class="my-4">
                            <h6 class="mb-3">Message</h6>
                            <form>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1" class="sr-only">Your Message</label>
                                    <textarea class="form-control bg-light" id="exampleFormControlTextarea1" rows="2"></textarea>
                                </div>
                                <div class="d-flex justify-content-between align-items-center">
                                    <!-- <div class="form-check form-check-inline ml-1">
                                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        <label class="form-check-label" for="inlineCheckbox1">Email Notification</label>
                                                    </div> -->
                                    <div class="flex-fill mr-2 text-right">
                                        <a href="#" class="btn"><i class="fe fe-upload"></i></a>
                                        <a href="#" class="btn"><i class="fe fe-at-sign"></i></a>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div> <!-- .card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>