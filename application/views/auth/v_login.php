<body class="light ">
  <div class="wrapper vh-100">
    <div class="row align-items-center h-100">
      <form class="col-lg-3 col-md-4 col-10 mx-auto text-center" action="<?php echo base_url(); ?>c_login" method="post">
        <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="<?php echo base_url(); ?>c_login">
          <svg version="1.1" id="logo" class="navbar-brand-img brand-md" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
            <g>
              <polygon class="st0" points="78,105 15,105 24,87 87,87 	" />
              <polygon class="st0" points="96,69 33,69 42,51 105,51 	" />
              <polygon class="st0" points="78,33 15,33 24,15 87,15 	" />
            </g>
          </svg>
        </a>
        <h1 class="h6 mb-3">Sign in</h1>

        <?php
        if ($this->session->flashdata('msg')) {
        ?>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><span class="fe fe-minus-circle fe-16 mr-2"></span> <?php echo $this->session->flashdata('msg'); ?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <?php
        }
        ?>

        <div class="form-group">
          <label for="inputEmail" class="sr-only">Email address</label>
          <input type="email" id="inputEmail" class="form-control form-control-lg" placeholder="Email address" required="" autofocus="" name="email" value="<?php echo set_value('email'); ?>">
        </div>
        <div class="form-group">
          <label for="inputPassword" class="sr-only">Password</label>
          <input type="password" id="inputPassword" class="form-control form-control-lg" placeholder="Password" required="" name="password" value="<?php echo set_value('password'); ?>">
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit">Log in</button>
        <p class="mt-3 mb-3 text-muted">OR</p>
        <a href="<?php echo base_url(); ?>c_register" class="btn btn-lg btn-primary btn-block" type="submit">Register</a>
        <!-- <a href="<?php echo base_url(); ?>c_dashboard" class="btn btn-lg btn-success btn-block" type="submit">Log in</a>
          <p class="mt-3 mb-3 text-muted">OR</p> -->
      </form>
    </div>
  </div>