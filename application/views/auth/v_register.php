<body class="light ">
  <div class="wrapper vh-100">
    <div class="row align-items-center h-100">
      <form class="col-lg-6 col-md-8 col-10 mx-auto">
        <div class="mx-auto text-center my-4">
          <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="./index.html">
            <svg version="1.1" id="logo" class="navbar-brand-img brand-md" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 120 120" xml:space="preserve">
              <g>
                <polygon class="st0" points="78,105 15,105 24,87 87,87 	" />
                <polygon class="st0" points="96,69 33,69 42,51 105,51 	" />
                <polygon class="st0" points="78,33 15,33 24,15 87,15 	" />
              </g>
            </svg>
          </a>
          <h2 class="my-3">Register</h2>
        </div>
        <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-akun-tab" data-toggle="pill" href="#pills-akun" role="tab" aria-controls="pills-akun" aria-selected="true">Akun</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="pills-package-tab" data-toggle="pill" href="#pills-package" role="tab" aria-controls="pills-package" aria-selected="true">Paket Subscribe</a>
          </li>
        </ul>
        <div class="tab-content mb-1" id="pills-tabContent">
          <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="form-group">
              <label for="inputNama">Nama Lengkap</label>
              <div class="input-group">
                <input type="text" class="form-control" id="inputNama" placeholder="Nama Lengkap">
                <div class="input-group-append">
                  <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNomor">Nomor KTP</label>
              <div class="input-group">
                <input type="text" class="form-control" id="inputNomor" placeholder="Nomor KTP">
                <div class="input-group-append">
                  <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputNomor">Nomor Handphone</label>
              <div class="input-group">
                <input type="text" class="form-control" id="inputNomor" placeholder="08xxxxxxx">
                <div class="input-group-append">
                  <div class="input-group-text" id="button-addon-date"><span class="fe fe-user fe-16"></span></div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="inputAddress">Alamat Lengkap</label>
              <textarea name="client_address" class="form-control" id="inputAddress" cols="30" rows="10" placeholder="Jl. Swadaya II No.20, RW.9, Sukamaju Baru, Kec. Tapos, Kota Depok, Jawa Barat 16455"></textarea>
            </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="inputCity">Provinsi</label>
                <select id="inputState5" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="inputCity">Kota</label>
                <select id="inputState5" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="inputState">Kecamatan</label>
                <select id="inputState5" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label for="inputZip">Kelurahan</label>
                <select id="inputState5" class="form-control">
                  <option selected>Choose...</option>
                  <option>...</option>
                </select>
              </div>
            </div>
            <div class="form-group mb-3">
              <label for="customFile">Foto/Avatar</label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="form-group mb-3">
              <label for="customFile">Foto Scan KTP</label>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="pills-akun" role="tabpanel" aria-labelledby="pills-akun-tab">
            <div class="form-group">
              <label for="inputEmail4">Email</label>
              <div class="input-group">
                <input type="email" class="form-control" id="inputEmail5">
                <div class="input-group-append">
                  <div class="input-group-text" id="button-addon-date"><span class="fe fe-mail fe-16"></span></div>
                </div>
              </div>
            </div>
            <hr class="my-4">
            <div class="row mb-4">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="inputPassword4">Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" id="inputPassword5">
                    <div class="input-group-append">
                      <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword4">Konfirmasi Password</label>
                  <div class="input-group">
                    <input type="password" class="form-control" id="inputPassword5">
                    <div class="input-group-append">
                      <div class="input-group-text" id="button-addon-date"><span class="fe fe-lock fe-16"></span></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <p class="mb-2">Password requirements</p>
                <p class="small text-muted mb-2"> To create a new password, you have to meet all of the following requirements: </p>
                <ul class="small text-muted pl-4 mb-0">
                  <li> Minimum 8 character </li>
                  <li>At least one special character</li>
                  <li>At least one number</li>
                  <li>Can’t be the same as a previous password </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="tab-pane fade" id="pills-package" role="tabpanel" aria-labelledby="pills-package-tab">
            <div class="card-deck my-4">
              <div class="card mb-4 shadow">
                <div class="card-body text-center my-4">
                  <a href="#">
                    <h3 class="h5 mt-4 mb-0">Basic</h3>
                  </a>
                  <p class="text-muted">package</p>
                  <span class="h1 mb-0">$9.9</span>
                  <p class="text-muted">month</p>
                  <ul class="list-unstyled">
                    <li>Lorem ipsum dolor sit amet</li>
                  </ul>
                  <span class="dot dot-lg bg-success"></span>
                  <span class="text-muted ml-3">Active</span>
                </div> <!-- .card-body -->
              </div> <!-- .card -->
              <div class="card mb-4">
                <div class="card-body text-center my-4">
                  <a href="#">
                    <h3 class="h5 mt-4 mb-0">Professional</h3>
                  </a>
                  <p class="text-muted">package</p>
                  <span class="h1 mb-0">$16.9</span>
                  <p class="text-muted">month</p>
                  <ul class="list-unstyled">
                    <li>Lorem ipsum dolor sit amet</li>
                  </ul>
                  <button type="button" class="btn mb-2 btn-primary btn-lg">Upgrade</button>
                </div> <!-- .card-body -->
              </div> <!-- .card -->
              <div class="card mb-4">
                <div class="card-body text-center my-4">
                  <a href="#">
                    <h3 class="h5 mt-4 mb-0">Expert</h3>
                  </a>
                  <p class="text-muted">package</p>
                  <span class="h1 mb-0">$16.9</span>
                  <p class="text-muted">month</p>
                  <ul class="list-unstyled">
                    <li>Lorem ipsum dolor sit amet</li>
                  </ul>
                  <button type="button" class="btn mb-2 btn-primary btn-lg">Upgrade</button>
                </div> <!-- .card-body -->
              </div> <!-- .card -->
            </div> <!-- .card-group -->
          </div>
          <a href="<?php echo base_url(); ?>c_login" class="btn btn-lg btn-success btn-block" type="submit">Sign up</a>
          <p class="mt-3 mb-3 text-muted text-center">OR</p>
          <a href="<?php echo base_url(); ?>c_login" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</a>
        </div>
      </form>
    </div>
  </div>