<main role="main" class="main-content">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h2 class="h3 mb-4 page-title">Profil</h2>
                <div class="row mt-5 align-items-center">
                    <div class="col-md-3 text-center mb-5">
                        <div class="avatar avatar-xl">
                            <img src="<?php echo base_url() ?>tinydash_assets/assets/avatars/face-1.jpg" alt="..." class="avatar-img rounded-circle">
                        </div>
                    </div>
                    <div class="col">
                        <div class="row align-items-center">
                            <div class="col-md-7">
                                <h4 class="mb-1"><?php echo $this->session->userdata('ses_nama'); ?></h4>
                                <p class="small mb-3"><span class="badge badge-dark"><?php echo $this->session->userdata('ses_address'); ?></span></p>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-md-7">
                                <p class="text-muted"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit nisl ullamcorper, rutrum metus in, congue lectus. In hac habitasse platea dictumst. Cras urna quam, malesuada vitae risus at, pretium blandit sapien. </p>
                            </div>
                            <div class="col">
                                <p class="small mb-0 text-muted"><?php echo $this->session->userdata('ses_ktp'); ?></p>
                                <p class="small mb-0 text-muted"><?php echo $this->session->userdata('ses_email'); ?></p>
                                <p class="small mb-0 text-muted"><?php echo $this->session->userdata('ses_phone'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-md-6">
                        <div class="card mb-4 shadow">
                            <div class="card-body my-n3">
                                <div class="row align-items-center">
                                    <div class="col-3 text-center">
                                        <span class="circle circle-lg bg-light">
                                            <i class="fe fe-user fe-24 text-primary"></i>
                                        </span>
                                    </div> <!-- .col -->
                                    <div class="col">
                                        <a href="#">
                                            <h3 class="h5 mt-4 mb-1">Akun</h3>
                                        </a>
                                        <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit nisl ullamcorper, rutrum metus in, congue lectus.</p>
                                    </div> <!-- .col -->
                                </div> <!-- .row -->
                            </div> <!-- .card-body -->
                            <div class="card-footer">
                                <a href="" class="d-flex justify-content-between text-muted"><span>Pengaturan Akun</span><i class="fe fe-chevron-right"></i></a>
                            </div> <!-- .card-footer -->
                        </div> <!-- .card -->
                    </div> <!-- .col-md-->
                </div> <!-- .row-->
                <h3>Subscription</h3>
                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit nisl ullamcorper, rutrum metus in, congue lectus.</p>
                <div class="card-deck my-4">
                    <div class="card mb-4 shadow">
                        <div class="card-body text-center my-4">
                            <a href="#">
                                <h3 class="h5 mt-4 mb-0">Basic</h3>
                            </a>
                            <p class="text-muted">package</p>
                            <span class="h1 mb-0">$9.9</span>
                            <p class="text-muted">year</p>
                            <ul class="list-unstyled">
                                <li>Lorem ipsum dolor sit amet</li>
                                <li>Consectetur adipiscing elit</li>
                                <li>Integer molestie lorem at massa</li>
                                <li>Eget porttitor lorem</li>
                            </ul>
                            <span class="dot dot-lg bg-success"></span>
                            <span class="text-muted ml-3">Active</span>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                    <div class="card mb-4">
                        <div class="card-body text-center my-4">
                            <a href="#">
                                <h3 class="h5 mt-4 mb-0">Professional</h3>
                            </a>
                            <p class="text-muted">package</p>
                            <span class="h1 mb-0">$16.9</span>
                            <p class="text-muted">year</p>
                            <ul class="list-unstyled">
                                <li>Lorem ipsum dolor sit amet</li>
                                <li>Consectetur adipiscing elit</li>
                                <li>Integer molestie lorem at massa</li>
                                <li>Eget porttitor lorem</li>
                            </ul>
                            <button type="button" class="btn mb-2 btn-primary btn-lg">Upgrade</button>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .card-group -->
            </div> <!-- /.col-12 -->
        </div> <!-- .row -->
    </div> <!-- .container-fluid -->