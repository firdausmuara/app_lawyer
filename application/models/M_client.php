<?php
class M_client extends CI_Model
{

    private $_table = "l_client_member";
    private $_bukti = "l_list_bukti";

    public $client_name;
    public $client_email;
    public $client_phone;
    public $client_address;
    public $province;
    public $kab_kota;
    public $kecamatan;
    public $kelurahan;
    public $avatar;
    public $lawyer_id;
    public $created_at;
    public $edited_at;
    public $password;
    public $status;

    public function rules()
    {
        return [
            [
                'field' => 'client_name',
                'label' => 'Name',
                'rules' => 'required'
            ],

            [
                'field' => 'client_email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ],

            [
                'field' => 'client_phone',
                'label' => 'Nomor Handphone',
                'rules' => 'required'
            ],

            [
                'field' => 'client_address',
                'label' => 'Alamat',
                'rules' => 'required'
            ],

            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|is_unique[l_client_member.client_email]'
            ],
        ];
    }

    function get_data()
    {
        $this->db->select('*');
        $this->db->select('l_client_member.password as client_password');
        $this->db->from($this->_table);
        $this->db->join('l_member_detail', 'l_member_detail.id = l_client_member.lawyer_id');
        $query = $this->db->get();
        return $query;
    }

    function get_data_by_lawyer($id_lawyer)
    {
        $hasil = $this->db->get_where($this->_table, ['lawyer_id' => $id_lawyer]);
        return $hasil;
    }

    function get_data_by_id($id_client)
    {
        $hasil = $this->db->get_where($this->_table, ['id_client' => $id_client]);
        return $hasil;
    }

    function get_bukti_by_id($id_client)
    {
        $hasil = $this->db->get_where($this->_bukti, ['id_client' => $id_client]);
        return $hasil;
    }

    public function save()
    {
        $post = $this->input->post();
        $this->client_name = $post["client_name"];
        $this->client_email = $post["client_email"];
        $this->client_phone = $post["client_phone"];
        $this->client_address = $post["client_address"];
        $this->province = $post["province"];
        $this->kab_kota = $post["kab_kota"];
        $this->kecamatan = $post["kecamatan"];
        $this->kelurahan = $post["kelurahan"];
        $this->avatar = $post["avatar"];
        $this->lawyer_id = $post["lawyer_id"];
        $this->created_at = $post["created_at"];
        $this->edited_at = $post["edited_at"];
        $this->password = $post["password"];
        $this->status = $post["status"];
        return $this->db->insert($this->_table, $this);
    }
}
