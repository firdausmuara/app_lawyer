<?php
class M_kasus extends CI_Model
{

    private $_table = "l_case_task";

    function get_data_by_lawyer($id_lawyer)
    {
        $hasil = $this->db->get_where($this->_table, ['lawyer_id' => $id_lawyer]);
        return $hasil;
    }

    function get_data()
    {
        $this->db->select('*');
        $this->db->from($this->_table);
        $query = $this->db->get();
        return $query;
    }

    function search_data_by_lawyer($id_lawyer, $limit, $start, $keyword = null)
    {
        $this->db->select('*, full_name as lawyer_name');
        $this->db->from($this->_table);
        $this->db->join('l_member_detail', 'l_member_detail.id = l_case_task.lawyer_id');
        $this->db->join('l_client_member', 'l_client_member.id_client = l_case_task.client_id');
        $this->db->where('l_case_task.lawyer_id', $id_lawyer);
        $this->db->limit($limit, $start);

        if ($keyword) {
            $this->db->like('case_name', $keyword);
            $this->db->or_like('case_type', $keyword);
        }

        $query = $this->db->get();
        return $query;
    }

    function search_data($limit, $start, $keyword = null)
    {
        $this->db->select('*, full_name as lawyer_name');
        $this->db->from($this->_table);
        $this->db->join('l_member_detail', 'l_member_detail.id = l_case_task.lawyer_id');
        $this->db->join('l_client_member', 'l_client_member.id_client = l_case_task.client_id');
        $this->db->limit($limit, $start);

        if ($keyword) {
            $this->db->like('case_name', $keyword);
            $this->db->or_like('case_type', $keyword);
        }

        $query = $this->db->get();
        return $query;
    }

    public function countAll()
    {
        return $this->db->get($this->_table)->num_rows();
    }
}
