<?php
class M_lawyer extends CI_Model
{

    private $_table = "l_member_detail";
    private $_dasar_hukum = "l_pasal_kuhp";

    public $client_name;
    public $client_email;
    public $client_phone;
    public $client_address;
    public $province;
    public $kab_kota;
    public $kecamatan;
    public $kelurahan;
    public $avatar;
    public $lawyer_id;
    public $created_at;
    public $edited_at;
    public $password;
    public $status;

    public function rules()
    {
        return [
            [
                'field' => 'full_name',
                'label' => 'Name',
                'rules' => 'required'
            ],

            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ],

            [
                'field' => 'client_phone',
                'label' => 'Nomor Handphone',
                'rules' => 'required'
            ],

            [
                'field' => 'client_address',
                'label' => 'Alamat',
                'rules' => 'required'
            ],

            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|is_unique[l_member_detail.email]'
            ],
        ];
    }

    function get_data()
    {
        $this->db->select('*');
        $this->db->select('l_package.package_name as paket_tipe');
        $this->db->from($this->_table);
        $this->db->join('l_package', 'l_package.id_package = l_member_detail.package_type');
        $query = $this->db->get();
        return $query;
    }

    function get_dasar_hukum()
    {
        $this->db->select('*');
        $this->db->from($this->_dasar_hukum);
        $query = $this->db->get();
        return $query;
    }

    public function save()
    {
        $post = $this->input->post();
        $this->client_name = $post["client_name"];
        $this->client_email = $post["client_email"];
        $this->client_phone = $post["client_phone"];
        $this->client_address = $post["client_address"];
        $this->province = $post["province"];
        $this->kab_kota = $post["kab_kota"];
        $this->kecamatan = $post["kecamatan"];
        $this->kelurahan = $post["kelurahan"];
        $this->avatar = $post["avatar"];
        $this->lawyer_id = $post["lawyer_id"];
        $this->created_at = $post["created_at"];
        $this->edited_at = $post["edited_at"];
        $this->password = $post["password"];
        $this->status = $post["status"];
        return $this->db->insert($this->_table, $this);
    }
}
